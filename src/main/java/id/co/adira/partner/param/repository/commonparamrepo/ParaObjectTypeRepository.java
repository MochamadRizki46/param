package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaObjectType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaObjectTypeRepository extends JpaRepository<ParaObjectType, String> {
    @Query("select pot.paraObjectTypeId " +
            "from ParaObjectType pot " +
            "join ParaObjectGroup pog on " +
            "pot.paraObjectGroup = pog.paraObjectGroupId " +
            "join ParaObject po on " +
            "pog.paraObjectGroupId = po.paraObjectGroup " +
            "where po.paraObjectId = :objectId")
    String getParaObjectTypeIdByObjectId(String objectId);
}
