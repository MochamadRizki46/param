package id.co.adira.partner.param.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoggerEnum {

    SUCCESS_GET_KELURAHAN("Success getting kelurahan from DB. Data: {}"),
    SUCCESS_GET_KECAMATAN("Success getting kecamatan from DB. Data: {}"),
    SUCCESS_GET_KABKOT("Success getting kabkot from DB. Data: {}"),
    SUCCESS_GET_PORTFOLIO("Success getting portfolio from DB. Data: {}"),
    SUCCESS_GET_BRAND("Success getting brand from DB. Data: {}"),
    SUCCESS_GET_EFFECTIVE_OR_LANDING_RATE("Success getting effective or landing rate. Data: {}"),
    SUCCESS_GET_BIAYA_ADMIN("Success getting biaya admin  from DB. Data: {}"),
    SUCCESS_GET_INSURANCE_RATE("Success getting insurance rate from DB. Data: {}"),
    SUCCESS_GET_DATA_PROGRAM("Success getting data program from DB. Data: {}"),
    SUCCESS_GET_TIPE_UNIT("Success getting tipe unit from DB. Data: {}"),
    SUCCESS_GET_MODEL_DETAIL("Success getting model detail from DB. Data: {}"),
    SUCCESS_GET_MIN_MAX_VALUE_PENCAIRAN("Success getting min and max value pencairan from DB. Data: {}"),
    SUCCESS_GET_LOV_BTM("Success getting LOV BTM. Data: {}"),
    SUCCESS_CALCULATE_CICILAN("Success calculate cicilan. Data: {}"),
    SUCCESS_GET_ASURANSI_CASH_AND_CREDIT("Success getting asuransi cash and credit. Data: {}"),
    SUCCESS_GET_MODEL("Success getting model. Data: {}"),
    SUCCESS_GET_DP_VALUE_FOR_DATUN("Success getting DP Value from DB. Data: {}"),
    SUCCESS_GET_OTR_VALUE_FROM_LTV("Success getting OTR value from LTV. Data: {}"),

    ERROR_WHILE_GET_KELURAHAN("Error while getting kelurahan: {}"),
    ERROR_WHILE_GET_KECAMATAN("Error while getting kecamatan: {}"),
    ERROR_WHILE_GET_KABKOT("Error while getting kabkot: {}"),
    ERROR_WHILE_GET_USER_DETAIL("Error while getting user detail: {}"),
    ERROR_WHILE_GET_BRAND("Error while getting brand: {}"),
    ERROR_WHILE_GET_EFFECTIVE_OR_LANDING_RATE("Error while getting effective or landing rate: {}"),
    ERROR_WHILE_GET_BIAYA_ADMIN("Error while getting biaya admin: {}"),
    ERROR_WHILE_GET_INSURANCE_RATE("Error while getting insurance rate: {} "),
    ERROR_WHILE_GET_DATA_PROGRAM("Error while getting data program: {}"),
    ERROR_WHILE_GET_TIPE_UNIT("Error while getting tipe unit: {}"),
    ERROR_WHILE_GET_MODEL_DETAIL("Error while getting model detail: {}"),
    ERROR_WHILE_GET_MIN_MAX_VALUE_PENCAIRAN("Error while getting min and max value pencairan: {}"),
    ERROR_WHILE_GET_LOV_BTM("Error while getting LOV BTM: {}"),
    ERROR_WHILE_CALCULATE_CICILAN("Error while calculate cicilan: {}"),
    ERROR_WHILE_GET_ASURANSI_CASH_AND_CREDIT("Error while getting asuransi cash and credit: {}"),
    ERROR_WHILE_GET_MODEL("Error while getting model: {}"),
    ERROR_WHILE_GET_OTR("Error while getting OTR: {}"),
    ERROR_WHILE_GET_INSTALLMENT_TYPE("Error while getting data installment type: {}"),
    ERROR_WHILE_GETTING_DP_VALUE_FOR_DATUN("Error while getting DP value for datun: {}");


    private String message;

}
