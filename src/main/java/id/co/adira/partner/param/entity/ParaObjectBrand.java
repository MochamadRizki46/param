package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_object_brand")
@Data
public class ParaObjectBrand {

    @Id
    @Column(name = "para_object_brand_id")
    String paraObjectBrandId;

    @Column(name = "para_object_made_in_id")
    String paraObjectMadeInId;

    @Column(name = "para_object_brand_name")
    String paraObjectBrandName;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @Column(name = "visible")
    Integer visible;

    @ManyToOne
    @JoinColumn(name = "para_object_group_id")
    ParaObjectGroup paraObjectGroup;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<ParaObjectModel> paraObjectModelList;

}
