package id.co.adira.partner.param.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_object_type")
public class ParaObjectType {

    @Id
    @Column(name = "para_object_type_id")
    String paraObjectTypeId;

    @Column(name = "para_object_type_name")
    String paraObjectTypeName;

    @Column(name = "effectiveDate")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @Column(name = "visible")
    Integer visible;

    @ManyToOne
    @JoinColumn(name = "para_object_group_id")
    ParaObjectGroup paraObjectGroup;

}
