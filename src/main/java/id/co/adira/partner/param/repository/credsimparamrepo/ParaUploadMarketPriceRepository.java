package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.BatasAtasAndBatasBawahResponseDTO;
import id.co.adira.partner.param.dto.response.MinMaxPencairanResponseDTO;
import id.co.adira.partner.param.dto.response.MarketPriceResponseDTO;
import id.co.adira.partner.param.entity.ParaUploadMarketPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public interface ParaUploadMarketPriceRepository extends JpaRepository<ParaUploadMarketPrice, String> {

    @Query("select new id.co.adira.partner.param.dto.response.BatasAtasAndBatasBawahResponseDTO(pul.paraBatasAtasRate, pul.paraBatasAtasRate, pul.paraOtr) " +
            "from ParaUploadLtv pul inner join ParaUploadMapRegionKota pumrk on " +
            "pul.paraCpAreaId = pumrk.paraCpAreaId " +
            "where pul.paraObjectModelId = :objectModelId " +
            "and pul.paraYear = :manufacturerYear " +
            "and pumrk.paraKabKotId = :kabKotId " +
            "and pumrk.active = 0 " +
            "and current_timestamp between pul.startDate and pul.endDate " +
            "and pul.active = 0 " +
            "order by pul.createdDate desc")
    List<BatasAtasAndBatasBawahResponseDTO> getOtrFromLtv(String manufacturerYear, String kabKotId, String objectModelId);

    @Query("select new id.co.adira.partner.param.dto.response.MinMaxPencairanResponseDTO(pump.paraObjectBrandName, pump.paraObjectGroupName, pump.paraObjectModelName, pump.paraMfgYear, pump.paraMarketPrice, pump.paraMarketPrice) " +
            "from ParaUploadMarketPrice pump join ParaObjectType pot on " +
            "pump.paraObjectTypeId = pot.paraObjectTypeId join ParaObjectGroup pog on " +
            " pot.paraObjectGroup = pog.paraObjectGroupId where " +
            "pump.paraObjectBrandId = :objectBrandId and pump.paraMfgYear = :manufacturerYear " +
            "and pump.paraObjectModelId = :objectModelId " +
            "and lower(pump.paraObjectGroupName) = :tipeKendaraan " +
            "and lower(pog.paraObjectGroupName) = :tipeKendaraan " +
            "order by pump.createdDate desc")
    List<MinMaxPencairanResponseDTO> getMinMaxFromMarketPlace(String objectModelId, String objectBrandId, String tipeKendaraan, BigDecimal manufacturerYear);

    @Query("select new id.co.adira.partner.param.dto.response.MarketPriceResponseDTO(pump.paraObjectTypeName, pump.paraObjectBrandName, pump.paraObjectGroupName, pump.paraObjectModelName, pump.paraMfgYear, pump.paraMarketPrice) " +
            "from ParaUploadMarketPrice pump " +
            "join ParaObjectGroup pog on " +
            "pump.paraObjectGroupId = pog.paraObjectGroupId " +
            "join ParaObject po on " +
            "pog.paraObjectGroupId = po.paraObjectGroup.paraObjectGroupId " +
            "where pump.paraObjectBrandId = :objectBrandId " +
            "and pump.paraObjectTypeId = :objectTypeId " +
            "and pump.paraObjectModelId = :objectModelId " +
            "and pump.paraMfgYear = :manufacturerYear " +
            "and pog.paraObjectGroupId = :objectGroupId " +
            "order by pump.createdDate desc")
    List<MarketPriceResponseDTO> getOtr(String objectGroupId, String objectModelId, String objectTypeId, String objectBrandId, BigDecimal manufacturerYear);
    
}
