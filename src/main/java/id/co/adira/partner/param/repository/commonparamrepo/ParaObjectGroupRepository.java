package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaObject;
import id.co.adira.partner.param.entity.ParaObjectGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaObjectGroupRepository extends JpaRepository<ParaObjectGroup, String> {

    @Query("select pog.paraObjectGroupId " +
            "from ParaObject po join ParaObjectGroup pog on " +
            "po.paraObjectGroup = pog.paraObjectGroupId " +
            "where po.paraObjectId = :objectId")
    String getParaObjectGroupByParaObjectId(String objectId);
}
