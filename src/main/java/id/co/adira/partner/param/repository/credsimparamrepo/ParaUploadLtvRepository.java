package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.OtrResponseDTO;
import id.co.adira.partner.param.entity.ParaUploadLtv;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public interface ParaUploadLtvRepository extends JpaRepository<ParaUploadLtv, String> {
    @Query("select new id.co.adira.partner.param.dto.response.OtrResponseDTO(pul.paraOtr, pul.paraBatasAtasRate) " +
            "from ParaUploadLtv pul where pul.paraObjectModelId = :objectModelId " +
            "and pul.paraYear = :manufacturerYear order by pul.createdDate desc")
    List<OtrResponseDTO> findAllOtr(String objectModelId, String manufacturerYear);
}
