package id.co.adira.partner.param.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ObjectConverter {

    ObjectMapper objectMapper = new ObjectMapper();

    public String convertObjectToString(Object data) throws JsonProcessingException {
        return objectMapper.writeValueAsString(data);
    }
}
