package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_kelurahan")
@Data
public class ParaKelurahan {

    @Id
    @Column(name = "para_kelurahan_id")
    String paraKelurahanId;

    @Column(name = "para_kelurahan_name")
    String paraKelurahanName;

    @Column(name = "para_zipcode")
    String paraZipCode;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "para_kecamatan_id")
    ParaKecamatan paraKecamatan;
}
