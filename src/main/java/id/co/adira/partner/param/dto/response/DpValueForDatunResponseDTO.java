package id.co.adira.partner.param.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DpValueForDatunResponseDTO {
    private BigDecimal marketPriceValue;
    private BigDecimal dpValue;

}
