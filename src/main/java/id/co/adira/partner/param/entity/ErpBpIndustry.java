package id.co.adira.partner.param.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "erp_bp_industry")
@Data
public class ErpBpIndustry {

    @Id
    @Column(name = "er_bp_industry_id")
    String erBpIndustryId;

    @Column(name = "erp_bp_bpartner")
    private String erpBpBpartner;

    @Column(name = "erp_bp_keysystem")
    private String erpBpkeySystem;

    @Column(name = "erp_bp_ind_sector")
    private String erpBpIndSector;

    @Column(name = "erp_bp_ind_default")
    private String erpBpIndDefault;

    @Column(name = "erp_bp_request_number")
    private BigDecimal erpBpRequestNumber;

    @Column(name = "erp_bp_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date erpBpTimestamp;

    @Column(name = "erp_bp_item_number")
    Integer erpBpItemNumber;

}

