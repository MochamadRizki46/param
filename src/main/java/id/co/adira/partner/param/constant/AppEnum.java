package id.co.adira.partner.param.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppEnum {
    SUCCESS("Success"),
    FAILED("Failed"),
    THERE_SOMETHING_ERROR_IN_OUR_SYSTEM("There's something error in our system!"),
    API_COMING_SOON("This API is Coming Soon..."),
    DATA_MARKETPRICE_NOT_FOUND("Market Price Data Not Found!"),
    DATA_OBJECT_MODEL_NOT_FOUND("Data Object Model Not Found!"),
    DATA_DEALER_NOT_FOUND("Data Dealer Not Found!"),
    PRODUCT_PROGRAM_NOT_FOUND("Product Program Not Found!"),
    KABKOT_NOT_FOUND("Kabupaten-Kota Data Not Found!"),
    BATAS_ATAS_DATA_NOT_FOUND("Batas Atas Data Not Found!"),
    OBJECT_GROUP_NOT_FOUND("Object Group Not Found!"),
    OBJECT_TYPE_NOT_FOUND("Object Type Not Found!"),
    OBJECT_BRAND_NOT_FOUND("Object Brand Not Found!"),
    OTR_NOT_FOUND("OTR Data Not Found!"),
    FOR_MOTOR_MOBIL_BEKAS_AND_DANA_TUNAI_ONLY("This request either not 'Motor Bekas', 'Mobil Bekas', or 'Dana Tunai'"),
    TENOR_INVALID("Tenor invalid. Your tenor value: "),
    ERROR_WHILE_GET_DATA("Error while get data: {}"),
    DATA_NOT_FOUND("Data Not Found: {}");

    private String message;
}
