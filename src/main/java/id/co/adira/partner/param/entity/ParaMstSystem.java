package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mst_system")
@Data
public class ParaMstSystem {

    @Id
    @Column(name = "system_code")
    private String systemCode;

    @Column(name = "system_type_code")
    private String systemTypeCode;

    @Column(name = "system_value")
    private String systemValue;

    @Column(name = "system_desc")
    private String systemDesc;

    @Column(name = "system_seq")
    Integer systemSequence;

    @Column(name = "disable_flag")
    Boolean disableFlag;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;


}
