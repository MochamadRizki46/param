package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaKecamatan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParaKecamatanRepository extends JpaRepository<ParaKecamatan, String> {
}
