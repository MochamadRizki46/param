package id.co.adira.partner.param.repository.commonparamrepo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.param.dto.response.PartnerIdResponseDTO;
import id.co.adira.partner.param.entity.ErpBpRole;

@Repository
public interface ErpBpRoleRepository extends JpaRepository<ErpBpRole, String> {

	@Query("select distinct new id.co.adira.partner.param.dto.response.PartnerIdResponseDTO(ebr.erpBpPartnerExternal, ebh.erpBpName1) "
			+ "from ErpBpRole ebr inner join ErpBpHeader ebh on ebh.erpBpBpartner = ebr.erpBpBpartner "
			+ "where ebr.erpBpRoleCategory = :roleCategoryBp and ebh.erpBpZstat <> 'Z' "
			+ "and (upper(ebh.erpBpName1) like :search " + "or ebr.erpBpPartnerExternal like :search)")
	List<PartnerIdResponseDTO> getPartnerIdBySearch(@Param("roleCategoryBp") String roleCategoryBp,
			@Param("search") String search);

}
