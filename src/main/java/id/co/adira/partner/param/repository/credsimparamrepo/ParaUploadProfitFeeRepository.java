package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.AsuransiCashAndCreditResponseDTO;
import id.co.adira.partner.param.dto.response.BiayaAdminResponseDTO;
import id.co.adira.partner.param.entity.ParaUploadProfitFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public interface ParaUploadProfitFeeRepository extends JpaRepository<ParaUploadProfitFee, String> {

    @Query("select new id.co.adira.partner.param.dto.response.BiayaAdminResponseDTO(sum(pupf.paraFeeCash), sum(pupf.paraFeeCredit)) " +
            "from ParaUploadProfit pup " +
            "inner join ParaUploadProfitDetail pupd on " +
            "pup.paraUploadProfitId = pupd.paraUploadProfit.paraUploadProfitId " +
            "inner join ParaUploadProfitFee pupf on " +
            "pupd.paraUploadProfitDetailId = pupf.paraUploadProfitDetail.paraUploadProfitDetailId " +
            "inner join ParaFeeType pft on " +
            "pupf.paraFeeTypeCode = pft.paraFeeTypeId " +
            "inner join ParaMstSystem pms on " +
            "pms.systemTypeCode = 'CREDSIM_ADIRA1PARTNER_OBJECTVSPRDMTX' " +
            "and pms.systemValue = pup.paraProdMatrixId " +
            "and pms.systemCode = :objectCode " +
            "where pup.paraUploadProfitType = 'PROFIT_TYPE_ADIRA' " +
            "and pup.paraProdProgramId = :programId " +
            "and pup.paraObjectModelId = :objectModelId " +
            "and pupd.paraTenor = :tenor " +
            "and current_timestamp between pup.startDate and pup.endDate and pup.active = 0 " +
            "and current_timestamp between pupd.startDate and pupd.endDate and pupd.active = 0 " +
            "and current_timestamp between  pupf.startDate and pupf.endDate and pupf.active = 0 " +
            "and pft.effectiveDate < current_timestamp and pft.active = 0" )
    BiayaAdminResponseDTO findBiayaAdminCashAndCreditForAutomotive(String programId, String objectModelId, BigDecimal tenor, String objectCode);

    @Query("select distinct new id.co.adira.partner.param.dto.response.AsuransiCashAndCreditResponseDTO(pupf.paraFeeCash, pupf.paraFeeCredit) " +
            "from ParaUploadProfit pup " +
            "inner join ParaUploadProfitDetail pupd on " +
            "pup.paraUploadProfitId = pupd.paraUploadProfit " +
            "inner join ParaUploadProfitFee pupf on " +
            "pupd.paraUploadProfitDetailId = pupf.paraUploadProfitDetail " +
            "inner join ParaMstSystem pms on " +
            "pupf.paraFeeTypeCode = pms.systemCode " +
            "inner join ParaMstSystem pms1 on " +
            "pms1.systemTypeCode = 'CREDSIM_ADIRA1PARTNER_OBJECTVSPRDMTX' " +
            "and pms1.systemValue = pup.paraProdMatrixId " +
            "and pms1.systemCode = :objectCode " +
            "where pup.paraUploadProfitType = 'PROFIT_TYPE_ADIRA' " +
            "and pup.paraProdProgramId = :programId " +
            "and pup.paraObjectModelId = :objectModelId " +
            "and pupd.paraTenor = :tenor " +
            "and pms.systemTypeCode = 'JENIS_BIAYA_UNGGAH' " +
            "and current_timestamp between pup.startDate and pup.endDate and pup.active = 0 " +
            "and current_timestamp between pupd.startDate and pupd.endDate and pupd.active = 0 " +
            "and current_timestamp between pupf.startDate and pupf.endDate and pupf.active = 0"
            )
    List<AsuransiCashAndCreditResponseDTO> findAsuransiCashAndCreditRate
            (String programId, String objectModelId, BigDecimal tenor, String objectCode);

    @Query("select new id.co.adira.partner.param.dto.response.BiayaAdminResponseDTO(sum(pupf.paraFeeCash), sum(pupf.paraFeeCredit) ) " +
            "from ParaUploadProfit pup " +
            "inner join ParaUploadProfitDetail pupd on " +
            "pup.paraUploadProfitId = pupd.paraUploadProfit.paraUploadProfitId " +
            "inner join ParaUploadProfitFee pupf on " +
            "pupd.paraUploadProfitDetailId = pupf.paraUploadProfitDetail.paraUploadProfitDetailId " +
            "inner join ParaFeeType pft on " +
            "pupf.paraFeeTypeCode = pft.paraFeeTypeId " +
            "where pup.paraUploadProfitType = 'PROFIT_TYPE_ADIRA' " +
            "and pup.paraProdProgramId = :programId " +
            "and pup.paraObjectModelId = :objectModelId " +
            "and pupd.paraTenor = :tenor " +
            "and current_timestamp between pup.startDate and pup.endDate and pup.active = 0 " +
            "and current_timestamp between pupd.startDate and pupd.endDate and pupd.active = 0 " +
            "and current_timestamp between pupf.startDate and pupf.endDate and pupf.active = 0 " +
            "and pft.effectiveDate < current_timestamp and pft.active = 0 " +
            "and pft.paraFeeTypeId = '01' " +
            "group by pup.paraObjectOtr")
    BiayaAdminResponseDTO findBiayaAdminCashAndCreditForDurable(String programId, String objectModelId, BigDecimal tenor);
}
