package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MinMaxPencairanRequestDTO {

    String objectModelId;

    String tipeKendaraan;

    @NotEmpty(message = "Object Brand Id can't be empty!")
    String objectBrandId;

    @NotEmpty(message = "Manufacturer Year can't be empty!")
    String manufacturerYear;

    String kabKotId;

}
