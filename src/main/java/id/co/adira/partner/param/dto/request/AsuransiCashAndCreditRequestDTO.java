package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AsuransiCashAndCreditRequestDTO {

    private String programId;

    private String objectModelId;

    private String tenor;

    private String objectCode;

    private Double otr;

}
