package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.entity.ParaProductProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaProductProgramRepository extends JpaRepository<ParaProductProgram, String> {

    @Query("SELECT pp FROM ParaProductProgram pp WHERE pp.paraProductProgramId = :programId")
    ParaProductProgram getParaProductProgramByParaProductProgramId(String programId);
}
