package id.co.adira.partner.param.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParaProgramResponseDTO {
    private String paraProdProgramId;

    private String paraProdProgramName;

    private String effectiveDate;

}
