package id.co.adira.partner.param.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "para_upld_ltv")
public class ParaUploadLtv {
    @Id
    @Column(name = "para_upld_ltv_id")
    String paraUploadLtvId;

    @Column(name = "para_cp_area_id")
    String paraCpAreaId;

    @Column(name = "para_object_group_id")
    String paraObjectGroupId;

    @Column(name = "para_fin_type_id")
    Integer paraFinTypeId;

    @Column(name = "para_object_model_id")
    String paraObjectModelId;

    @Column(name = "para_year")
    String paraYear;

    @Column(name = "para_batas_atas_rate")
    BigDecimal paraBatasAtasRate;

    @Column(name = "para_otr")
    BigDecimal paraOtr;

    @Column(name = "para_dp_net_rate")
    BigDecimal paraDpNetRate;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

}
