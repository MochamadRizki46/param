package id.co.adira.partner.param.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BrandResponseDTO {
    String partnerCode;
    List<String> brandList;
}
