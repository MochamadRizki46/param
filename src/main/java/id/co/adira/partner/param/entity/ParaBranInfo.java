package id.co.adira.partner.param.entity;

import lombok.Data;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "para_bran_info")
@Data
public class ParaBranInfo {
    @Id
    @Column(name = "bran_company_id")
    String branCompanyId;

    @Unique
    @Column(name = "bran_br_id")
    String branBrId;

    @Column(name = "bran_parent_id")
    String branParentId;

    @Column(name = "bran_br_name")
    String branBrName;

    @Column(name = "bran_telph1")
    String branTelph1;

    @Column(name = "bran_telph2")
    String branTelph2;

    @Column(name = "bran_address1")
    String branAddress1;

    @Column(name = "bran_address2")
    String branAddress2;

    @Column(name = "bran_zip")
    String branZip;

    @Column(name = "bran_city")
    String branCity;

    @Column(name = "bran_bm_name")
    String branBmName;

    @Column(name = "bran_adh_name")
    String branAdhName;

    @Column(name = "bran_mh_name")
    String branMhName;

    @Column(name = "bran_pic_bpkb")
    String branPicBpkb;

    @Column(name = "bran_bpkb_period")
    Long branBpkbPeriod;

    @Column(name = "bran_bpkb_turnback_period")
    Long branBpkbTurnbackPeriod;

    @Column(name = "bran_pickup_period")
    Long branPickupPeriod;

    @Column(name = "bran_writeoff_period")
    Long branWriteOffPeriod;

    @Column(name = "bran_stop_accrue_period")
    Long branStopAccruePeriod;

    @Column(name = "bran_pnlt_type")
    String branPenaltyType;

    @Column(name = "bran_daily_pnlt_amnt_exp")
    BigDecimal branDailyPenaltyAmountExp;

    @Column(name = "bran_pnlt_rate_exp")
    String branPenaltyRateExp;

    @Column(name = "bran_memo")
    String branMemo;

    @Column(name = "bran_adm_fee")
    BigDecimal branAdmFee;

    @Column(name = "bran_insr_fee")
    BigDecimal branInsuranceFee;

    @Column(name = "bran_stamp_fee")
    BigDecimal branStampFee;

    @Column(name = "bran_net_dp_percent")
    Double branNetDpPercent;

    @Column(name = "bran_rounded_value")
    Long branRoundedValue;

    @Column(name = "bran_log_id")
    String branLogId;

    @Column(name = "bran_deleted")
    Integer branDeleted;

    @Column(name = "bran_af_name")
    String branAfName;

    @Column(name = "brand_lo_name")
    String branLoName;

    @Column(name = "bran_cp_name")
    String branCpName;

    @Column(name = "bran_aloc_name")
    String branAlocName;

    @Column(name = "bran_min_age")
    Long branMinAge;

    @Column(name = "bran_max_age")
    Long branMaxAge;

    @Column(name = "bran_migr_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date branMigrDate;

    @Column(name = "bran_prod_year_min")
    Long branProdYearMin;

    @Column(name = "bran_app_ver")
    String branAppVer;

    @Column(name = "bran_last_day_closing")
    @Temporal(TemporalType.TIMESTAMP)
    Date branLastDayClosing;

    @Column(name = "bran_pc_vch_max_limit")
    Integer branPchVchMaxLimit;

    @Column(name = "bran_pt_date_change")
    Integer branPtDateChange;

    @Column(name = "bran_pnlt_bdebt_1")
    BigDecimal branPenaltyBdebt1;

    @Column(name = "bran_pnlt_bdebt_rate_1")
    BigDecimal branPenaltyBdebtRate1;

    @Column(name = "bran_pnlt_bdebt_2")
    BigDecimal branPenaltyBdebt2;

    @Column(name = "bran_pnlt_bdebt_rate_2")
    BigDecimal branPenaltyBdebtRate2;

    @Column(name = "bran_pnlt_bdebt_3")
    BigDecimal branPenaltyBdebt3;

    @Column(name = "bran_pnlt_bdebt_rate_3")
    BigDecimal branPenaltyBdebtRate3;

    @Column(name = "bran_pnlt_bdebt_4")
    BigDecimal branPenaltyBdebt4;

    @Column(name = "bran_pnlt_bdebt_rate_4")
    BigDecimal branPenaltyBdebtRate4;

    @Column(name = "bran_pnlt_bdebt_5")
    BigDecimal branPenaltyBdebt5;

    @Column(name = "bran_pnlt_bdebt_rate_5")
    BigDecimal branPenaltyBdebtRate5;

    @Column(name = "bran_mtr_min_admin_fee")
    BigDecimal branMtrMinAdminFee;

    @Column(name = "bran_mbl_min_admin_fee")
    BigDecimal branMblMinAdminFee;

    @Column(name = "bran_insf_calc_mode")
    String branInsfCalcMode;

    @Column(name = "bran_fin_name")
    String branFinName;

    @Column(name = "bran_pnlt_target_1")
    BigDecimal branPenaltyTarget1;

    @Column(name = "bran_pnlt_target_rate_1")
    BigDecimal branPenaltyTargetRate1;

    @Column(name = "bran_pnlt_target_2")
    BigDecimal branPenaltyTarget2;

    @Column(name = "bran_pnlt_target_rate_2")
    BigDecimal branPenaltyTargetRate2;

    @Column(name = "bran_pnlt_target_3")
    BigDecimal branPenaltyTarget3;

    @Column(name = "bran_pnlt_target_rate_3")
    BigDecimal branPenaltyTargetRate3;

    @Column(name = "bran_pnlt_target_4")
    BigDecimal branPenaltyTarget4;

    @Column(name = "bran_pnlt_target_rate_4")
    BigDecimal branPenaltyTargetRate4;

    @Column(name = "bran_aging_od")
    Double branAgingOd;

    @Column(name = "bran_piut_kary_code")
    String branPiutKaryCode;

    @Column(name = "bran_os_drkh")
    Integer branOsDrkh;

    @Column(name = "bran_bpkb_loc")
    Integer branBpkbLoc;

    @Column(name = "bran_kpi_walr")
    Integer branKpiWalr;

    @Column(name = "bran_kpi_min_dp")
    BigDecimal branKpiMinDp;

    @Column(name = "bran_kpi_avg_insr")
    Integer branKpiAvgInsr;

    @Column(name = "bran_min_closing")
    Integer branMinClosing;

    @Column(name = "bran_max_upping_intr")
    Integer branMaxUppingIntr;

    @Column(name = "bran_max_pdh")
    Integer branMaxPdh;

    @Column(name = "bran_beg_deskcoll")
    Integer branBegDeskColl;

    @Column(name = "bran_end_deskcoll")
    Integer branEndDeskColl;

    @Column(name = "bran_beg_hr")
    Integer branBegHr;

    @Column(name = "bran_end_hr")
    Integer branEndHr;

    @Column(name = "bran_provinsi")
    String branProvinsi;

    @Column(name = "bran_queue_status")
    Integer branQueueStatus;

    @Column(name = "bran_custodian_name")
    String branCustodianName;

    @Column(name = "bran_custodian_address1")
    String branCustodianAddress1;

    @Column(name = "bran_custodian_address2")
    String branCustodianAddress2;

    @Column(name = "bran_insr_amt_limit")
    Long branInsrAmtLimit;

    @Column(name = "bran_month_corr_top")
    Long branMonthCorrTop;

    @Column(name = "bran_ibukota_provinsi")
    String branIbukotaProvinsi;

    @Column(name = "bran_pnlt_pnls")
    Long branPnltPnls;

    @Column(name = "bran_refund_adm_flag")
    String branRefundAdmFlag;

    @Column(name = "bran_bpkb_periode_amu")
    BigDecimal branBpkbPeriodeAmu;

    @Column(name = "bran_bins_rounded_value")
    Long branBinsRoundedValue;

    @Column(name = "bran_max_fdp")
    BigDecimal branMaxFdp;

    @Column(name = "bran_min_net_dp")
    BigDecimal branMinNetDp;

    @Column(name = "bran_max_tenor_limit")
    Long branMaxTenorLimit;

    @Column(name = "bran_min_adm_limit")
    Long branMinAdmLimit;

    @Column(name = "bran_mayor")
    BigDecimal branMayor;

    @Column(name = "bran_ora_cutoff_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date branOraCutoffDate;

    @Column(name = "bran_fdp")
    String branFdp;

    @Column(name = "bran_ms2_addr")
    String branMs2Addr;

    @Column(name = "bram_sys_lastday_closing")
    @Temporal(TemporalType.TIMESTAMP)
    Date branSysLastDayClosing;

    @Column(name = "bran_dsp_id")
    String branDspId;

    @Column(name = "bran_stop_closing")
    @Temporal(TemporalType.TIMESTAMP)
    Date branStopClosing;

    @Column(name = "bran_max_grace_period")
    BigDecimal branMaxGracePeriod;

    @Column(name = "bran_fleet")
    String branFleet;

    @Column(name = "bran_orafin_closing")
    @Temporal(TemporalType.TIMESTAMP)
    Date branOrafinClosing;

    @Column(name = "bran_disbursement_flag")
    String branDisbursementFlag;

    @Column(name = "bran_rate_renewal_insr")
    BigDecimal branRateRenewalInsr;

    @Column(name = "bran_closing_amore_fase1")
    @Temporal(TemporalType.TIMESTAMP)
    Date branClosingAmoreFase1;

    @Column(name = "bram_lastday_amor")
    @Temporal(TemporalType.TIMESTAMP)
    Date branLastDayAmor;

    @Column(name = "bran_area_opr")
    String branAreaOpr;

    @Column(name = "bran_monthly_closing")
    @Temporal(TemporalType.TIMESTAMP)
    Date branMonthlyClosing;

    @Column(name = "bran_lastday_sglnt_flag")
    @Temporal(TemporalType.TIMESTAMP)
    Date branLastDaySglntFlag;

    @Column(name = "bran_eom_ar_scoring")
    @Temporal(TemporalType.TIMESTAMP)
    Date branEomArScoring;

    @Column(name = "bran_max_count_ppd")
    Integer branMaxCountPpd;

    @Column(name = "bran_pool_id")
    @Temporal(TemporalType.TIMESTAMP)
    Date branPoolId;

    @Column(name = "bran_ms2_mcs")
    Double branMs2Mcs;

    @Column(name = "bran_eod_ar_scoring")
    @Temporal(TemporalType.TIMESTAMP)
    Date branEodArScoring;

    @Column(name = "bran_ovd_bpkb")
    Double branOvdBpkb;

    @Column(name = "bran_category")
    String branCategory;

    @Column(name = "bran_taxid_adira")
    String branTaxIdAdira;

    @Column(name = "bran_efektif_taxid_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date brandEfektifTaxIdDate;

    @Column(name = "bran_aloc_code_new")
    String branAlocCodeNew;

    @Column(name = "bran_sentraid")
    String branSentraId;

    @Column(name = "bran_region_id")
    String branRegionId;

    @Column(name = "bran_region_name")
    String branRegionName;

    @Column(name = "bran_region_acct_no")
    String branRegionAcctNo;

    @Column(name = "bran_region_bi_code")
    String branRegionBiCode;

    @Column(name = "bran_region_bank_city")
    String branRegionBankCity;

    @Column(name = "bran_region_bank_branch")
    String branRegionBankBranch;

    @Column(name = "bran_flag_fclos")
    String branFlagFclos;

    @Column(name = "bran_fclose_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date branFclosDate;

    @Column(name = "bran_eff_cops")
    @Temporal(TemporalType.TIMESTAMP)
    Date branEffCops;

    @Column(name = "bran_acct_start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date branAcctStartDate;

    @Column(name = "bran_acct_end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date branAcctEndDate;
}
