package id.co.adira.partner.param.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LovBrandTipeModelDetailResponseDTO {
    private String objectBrandId;

    private String objectBrandName;

    private String objectTypeId;

    private String objectTypeName;

    private String objectModelId;

    private String objectModelName;

    private String objectId;

    private String objectName;
}
