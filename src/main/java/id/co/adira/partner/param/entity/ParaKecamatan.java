package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_kecamatan")
@Data
public class ParaKecamatan {

    @Id
    @Column(name = "para_kecamatan_id")
    String paraKecamatanId;

    @Column(name = "para_kecamatan_name")
    String paraKecamatanName;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "para_kabkot_id")
    ParaKabKot paraKabKot;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "paraKecamatan")
    List<ParaKelurahan> paraKelurahanList;
}
