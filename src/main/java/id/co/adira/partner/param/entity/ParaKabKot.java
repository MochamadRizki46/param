package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_kabkot")
@Data
public class ParaKabKot {
   @Id
   @Column(name = "para_kabkot_id")
   String paraKabKotId;

   @Column(name = "para_kabkot_name")
   String paraKabKotName;

   @Column(name = "effective_date")
   @Temporal(TemporalType.TIMESTAMP)
   Date effectiveDate;

   @Column(name = "active")
   Integer active;

   @Column(name = "logid")
   Double logId;

   @Column(name = "created_date")
   @CreationTimestamp
   @Temporal(TemporalType.TIMESTAMP)
   Date createdDate;

   @Column(name = "created_by")
   String createdBy;

   @Column(name = "modified_date")
   @UpdateTimestamp
   @Temporal(TemporalType.TIMESTAMP)
   Date modifiedDate;

   @Column(name = "modified_by")
   String modifiedBy;

   @OneToMany(fetch = FetchType.EAGER, mappedBy = "paraKabKot")
   List<ParaKecamatan> paraKecamatanList;

   @ManyToOne
   @JoinColumn(name = "para_provinsi_id")
   ParaProvinsi paraProvinsi;
}
