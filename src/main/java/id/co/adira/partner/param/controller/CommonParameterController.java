package id.co.adira.partner.param.controller;

import id.co.adira.partner.param.dto.request.LOVBtmRequestDTO;
import id.co.adira.partner.param.dto.request.ModelRequestDTO;
import id.co.adira.partner.param.dto.request.PartnerIdRequestDTO;
import id.co.adira.partner.param.dto.request.TipeUnitRequestDTO;
import id.co.adira.partner.param.dto.response.BaseResponse;
import id.co.adira.partner.param.dto.response.BaseResponseNew;
import id.co.adira.partner.param.service.CommonParameterService;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/common/param")
public class CommonParameterController {

	private final CommonParameterService commonParameterService;

	@Autowired
	public CommonParameterController(CommonParameterService commonParameterService) {
		this.commonParameterService = commonParameterService;
	}

	@GetMapping("/get-kelurahan")
	@ApiOperation(value = "Get Kecamatan", nickname = "Get Kecamatan")
	//@CrossOrigin
	public ResponseEntity<Object> getKecamatan(@RequestParam String kelurahanName) {
		BaseResponse response = commonParameterService.getKecamatanByKelurahanName(kelurahanName);
		return new ResponseEntity<>(response, response.getStatus());
	}

	@GetMapping("/get-portfolio")
	@ApiOperation(value = "Get Portfolio", nickname = "Get Portfolio")
	//@CrossOrigin
	public ResponseEntity<Object> getPortfolio(@RequestParam String partnerCode) {
		BaseResponse response = commonParameterService.getPortfolioByPartnerCode(partnerCode);
		return new ResponseEntity<>(response, response.getStatus());
	}

	@GetMapping("/get-kabkot")
	@ApiOperation(value = "Get Kabupaten-Kota", nickname = "Get Kabupaten-Kota")
	//@CrossOrigin
	public ResponseEntity<Object> getKabKot(@RequestParam String kabKotName) {
		BaseResponse baseResponse = commonParameterService.getKabKotByName(kabKotName);
		return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
	}

	@PostMapping("/get-tipe-unit")
	@ApiOperation(value = "Get Tioe Unit", nickname = "Get Tipe Unit")
	//@CrossOrigin
	public ResponseEntity<Object> getTipeUnit(@RequestBody TipeUnitRequestDTO tipeUnitRequestDTO) {

		BaseResponse baseResponse = commonParameterService.getTipeUnit(tipeUnitRequestDTO);
		return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
	}

	@GetMapping("/get-brand-partner")
	@ApiOperation(value = "Get Brand Partner", nickname = "Get Brand Partner")
	//@CrossOrigin
	public ResponseEntity<Object> getBrandPartnerByPartnerCode(@RequestParam String partnerCode) {
		BaseResponse baseResponse = commonParameterService.getBrandPartnerByPartnerCode(partnerCode);
		return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
	}

	@PostMapping("/get-lov-brand-tipe-model")
	@ApiOperation(value = "Get LOV Brand Tipe Model", nickname = "Get LOV Brand Tipe Model")
	//@CrossOrigin
	public ResponseEntity<Object> getLOVBrandTipeModel(@RequestBody LOVBtmRequestDTO LOVBtmRequestDTO) {
		BaseResponse baseResponse = commonParameterService.getLOVBrandTipeModel(LOVBtmRequestDTO);
		return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
	}

	@GetMapping("/get-model-detail-durable")
	@ApiOperation(value = "Get Model Detail", nickname = "Get Model Detail")
	//@CrossOrigin
	public ResponseEntity<Object> getModelDetailDurable(@RequestParam String objectModelId) {
		BaseResponse baseResponse = commonParameterService.getModelDetailDurable(objectModelId);
		return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
	}

	@PostMapping("/get-model")
	@ApiOperation(value = "Get Model", nickname = "Get Model")
	//@CrossOrigin
	public ResponseEntity<Object> getModel(@RequestBody ModelRequestDTO modelRequestDTO) {
		BaseResponse baseResponse = commonParameterService.getModel(modelRequestDTO);
		return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
	}

	@PostMapping("/get-list-partnerid")
	@ApiOperation(value = "Get List Partner Id", nickname = "Get List Partner Id")
	public ResponseEntity<Object> getRoleByPartnerType(@Valid @RequestBody PartnerIdRequestDTO partnerIdRequest) {
		BaseResponseNew response = commonParameterService.getListPartner(partnerIdRequest);
		return new ResponseEntity<>(response, response.getHttpStatus());

	}

	@GetMapping("/get-maxtenor")
	@ApiOperation(value = "Get Max Tenor", nickname = "Get Max Tenor")
	//@CrossOrigin
	public ResponseEntity<Object> getMaxTenor() {
		BaseResponse response = commonParameterService.getMaxTenor();
		return new ResponseEntity<>(response, response.getStatus());
	}
}
