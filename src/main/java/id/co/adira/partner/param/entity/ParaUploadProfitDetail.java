package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_upld_profit_dtl")
@Data
public class ParaUploadProfitDetail {
    @Id
    @Column(name = "para_upld_profit_dtl_id")
    String paraUploadProfitDetailId;

    @Column(name = "para_tenor")
    BigDecimal paraTenor;

    @Column(name = "para_landing_rate")
    Double paraLandingRate;

    @Column(name = "para_inst_payment")
    Double paraInstPayment;

    @Column(name = "para_payment_type_id")
    String paraPaymentId;

    @Column(name = "para_tac_2")
    Double paraTac2;

    @Column(name = "para_tac_3")
    Double paraTac3;

    @Column(name = "para_insr_coc")
    Double paraInsrCoc;

    @Column(name = "profit_flag")
    boolean profitFlag;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @OneToOne
    @JoinColumn(name = "para_upld_profit_id")
    ParaUploadProfit paraUploadProfit;


}
