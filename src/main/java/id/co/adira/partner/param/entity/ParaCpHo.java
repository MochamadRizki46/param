package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_cp_ho")
@Data
public class ParaCpHo {

    @Id
    @Column(name = "para_cp_ho_id")
    String paraCpHoId;

    @Column(name = "para_cp_ho_name")
    String paraCpHoName;

    @Column(name = "para_address")
    String paraAddress;

    @Column(name = "para_rt")
    String paraRt;

    @Column(name = "para_rw")
    String paraRw;

    @Column(name = "para_kelurahan_id")
    String paraKelurahanId;

    @Column(name = "para_phone1_area")
    String paraPhone1Area;

    @Column(name = "para_phone1")
    String paraPhone1;

    @Column(name = "para_phone2_area")
    String paraPhone2Area;

    @Column(name = "para_phone2")
    String paraPhone2;

    @Column(name = "para_fax_area")
    String paraFaxArea;

    @Column(name = "para_fax")
    String paraFax;

    @Column(name = "para_ouid")
    String paraOuid;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "paraCpHo")
    List<ParaCpArea> paraCpArea;
}
