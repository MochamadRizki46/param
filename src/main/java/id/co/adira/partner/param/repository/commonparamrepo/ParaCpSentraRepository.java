package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaCpSentra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaCpSentraRepository extends JpaRepository<ParaCpSentra, String> {
}
