package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_object_group")
@Data
public class ParaObjectGroup {

    @Id
    @Column(name = "para_object_group_id")
    String paraObjectGroupId;

    @Column(name = "para_object_group_name")
    String paraObjectGroupName;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @Column(name = "visible")
    Integer visible;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "paraObjectGroup")
    List<ParaObject> paraObjectList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "paraObjectGroup")
    List<ParaObjectBrand> paraObjectBrands;

    @OneToMany(mappedBy = "paraObjectGroup")
    List<ParaObjectType> paraObjectTypes;
}
