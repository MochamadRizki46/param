package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_appl_status")
@Data
public class ParaApplStatus {

    @Id
    @Column(name = "para_appl_status_sk_id")
    String paraApplStatusSkId;

    @Column(name = "para_appl_status_id")
    String paraApplStatusId;

    @Column(name = "para_appl_status_desc")
    String paraApplStatusDesc;

    @Column(name = "squad")
    String squad;

    @Column(name = "level1")
    String level1;

    @Column(name = "level2")
    String level2;

    @Column(name = "level3")
    String level3;

    @Column(name = "effective_date")
    Date effectiveDate;

    @Column(name = "active")
    boolean active;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "para_appl_status_group_id")
    ParaApplStatusGroup paraApplStatusGroup;


}
