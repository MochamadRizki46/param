package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_appl_status_group")
@Data
public class ParaApplStatusGroup {
    @Id
    @Column(name = "para_appl_status_group_id")
    String paraApplStatusGroupId;

    @Column(name = "para_appl_status_group_desc")
    String paraApplStatusGroupDesc;

    @Column(name = "effective_date")
    Date effectiveDate;

    @Column(name = "active")
    boolean active;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "paraApplStatusGroup")
    List<ParaApplStatus> paraApplStatusList;
}
