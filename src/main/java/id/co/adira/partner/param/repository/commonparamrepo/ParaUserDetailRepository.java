package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaUserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
@Transactional
public interface ParaUserDetailRepository extends JpaRepository<ParaUserDetail, Long> {

    @Query("SELECT MIN(pud.portfolio) AS portfolio FROM ParaUserDetail pud WHERE pud.partnerCode = :partnerCode GROUP BY pud.portfolio")
    List<String> getPortfolioByPartnerCode(String partnerCode);

    @Query("SELECT MIN(pud.brand) AS brand FROM ParaUserDetail pud WHERE pud.partnerCode = :partnerCode GROUP BY pud.brand")
    List<String> getBrandByPartnerCode(String partnerCode);

    @Query("SELECT pud FROM ParaUserDetail pud WHERE pud.email = :username AND pud.partnerCode = :partnerCode")
    ParaUserDetail getParaUserDetailByUsernameAndPartnerCode(String username, String partnerCode);
    
    @Query("SELECT pud FROM ParaUserDetail pud WHERE pud.partnerCode = :partnerCode and pud.brand <> '' and pud.isActive = 0")
    List<ParaUserDetail> getUserDetailByDlc(@Param("partnerCode") String partnerCode);
}
