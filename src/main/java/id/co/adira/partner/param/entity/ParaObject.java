package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_object")
@Data
public class ParaObject {
    @Id
    @Column(name = "para_object_id")
    String paraObjectId;

    @Column(name = "para_object_name")
    String paraObjectName;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @Column(name = "visible")
    Integer visible;

    @ManyToOne
    @JoinColumn(name = "para_object_group_id")
    ParaObjectGroup paraObjectGroup;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paraObject")
    List<ParaProductMatrix> paraProductMatrixList;

}
