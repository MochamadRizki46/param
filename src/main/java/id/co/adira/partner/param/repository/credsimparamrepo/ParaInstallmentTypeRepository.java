package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.ParaInstallmentTypeResponseDTO;
import id.co.adira.partner.param.entity.ParaInstallmentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaInstallmentTypeRepository extends JpaRepository<ParaInstallmentType, String> {
    @Query("select new id.co.adira.partner.param.dto.response.ParaInstallmentTypeResponseDTO(a.paraInstallmentTypeId, a.paraInstallmentTypeName) from ParaInstallmentType a " +
    		"where a.active = 0")
    List<ParaInstallmentTypeResponseDTO> findParaInstallmentType();
}
