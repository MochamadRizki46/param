package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "para_upld_profit_fee")
@Data
public class ParaUploadProfitFee {
    @Id
    @Column(name = "para_upld_profit_fee_id")
    String paraUploadProfitFeeId;

    @Column(name = "para_fee_type_code")
    String paraFeeTypeCode;

    @Column(name = "para_fee_cash")
    BigDecimal paraFeeCash;

    @Column(name = "para_fee_credit")
    BigDecimal paraFeeCredit;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "para_upld_profit_dtl_id")
    ParaUploadProfitDetail paraUploadProfitDetail;
}
