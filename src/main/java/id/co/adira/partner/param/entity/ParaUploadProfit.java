package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "para_upld_profit")
@Data
public class ParaUploadProfit {
    @Id
    @Column(name = "para_upld_profit_id")
    String paraUploadProfitId;

    @Column(name = "para_upld_profit_type")
    String paraUploadProfitType;

    @Column(name = "para_cp_area_id")
    String paraCpAreaId;

    @Column(name = "para_kabkot_id")
    String paraKabKotId;

    @Column(name = "para_object_group_id")
    String paraObjectGroupId;

    @Column(name = "para_object_model_id")
    String paraObjectModelId;

    @Column(name = "para_object_model_detail_id")
    String paraObjectModelDetailId;

    @Column(name = "para_prod_matrix_id")
    String paraProdMatrixId;

    @Column(name = "para_prod_program_id")
    String paraProdProgramId;

    @Column(name = "para_object_otr")
    BigDecimal paraObjectOtr;

    @Column(name = "para_object_dp")
    Long paraObjectDp;

    @Column(name = "para_object_dp_subsidi")
    Long paraObjectDpSubsidi;

    @Column(name = "para_object_total_dp")
    Long paraObjectTotalDp;

    @Column(name = "para_tac_max")
    Long paraTacMax;

    @Column(name = "para_insr_cof")
    Long paraInsrCof;

    @Column(name = "para_insr_opex_acq")
    Long paraInsrOpexAcq;

    @Column(name = "para_insr_opex_mt")
    Long paraInsrOpexMt;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "paraUploadProfit")
    ParaUploadProfitDetail paraUploadProfitDetail;
}
