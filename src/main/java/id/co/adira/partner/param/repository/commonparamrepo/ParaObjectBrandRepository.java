package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaObjectBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaObjectBrandRepository extends JpaRepository<ParaObjectBrand, String> {

    @Query("select pob.paraObjectBrandId " +
            "from ParaObjectBrand pob " +
            "join ParaObjectModel pom on " +
            "pob.paraObjectBrandId = pom.paraObjectBrand " +
            "where pom.paraObjectModelId = :objectModelId")
    String getParaObjectBrandIdByObjectModelId(String objectModelId);
}
