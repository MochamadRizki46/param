package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DpValueForDanaTunaiRequestDTO {
    private String objectGroupId;

    private String objectModelId;

    private BigDecimal tenor;

    private String objectTypeId;

    private String objectBrandId;

    private BigDecimal manufacturerYear;

    private BigDecimal disbursementAmt;

}
