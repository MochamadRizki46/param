package id.co.adira.partner.param.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KecamatanDetailResponseDTO {
    @JsonProperty("PARA_KELURAHAN_ID")
    String paraKelurahanId;

    @JsonProperty("PARA_KELURAHAN_DESC")
    String paraKelurahanDesc;

    @JsonProperty("PARA_KELURAHAN_ID_KEC")
    String paraKelurahanIdKec;

    @JsonProperty("PARA_KECAMATAN_DESC")
    String paraKecamatanDesc;

    @JsonProperty("KAB_KOT_ID")
    String kabKotId;

    @JsonProperty("KAB_KOT_DESC")
    String kabKotDesc;

    @JsonProperty("KAB_KOT_ID_PROV")
    String kabKotIdProv;

    @JsonProperty("PROVINSI_DESC")
    String provinsiDesc;

    @JsonProperty("PARA_KELURAHAN_ZIP_CODE")
    String zipCode;
}
