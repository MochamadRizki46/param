package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "para_upld_landing_rate")
@Data
public class ParaUploadLandingRate {

    @Id
    @Column(name = "para_upld_landing_rate_id")
    String paraUploadLandingRateId;

    @Column(name = "para_cp_area_id")
    String paraCpAreaId;

    @Column(name = "para_kabkot_id")
    String paraKabKotId;

    @Column(name = "para_deal_outlet_id")
    String paraDealOutletId;

    @Column(name = "para_object_id")
    String paraObjectId;

    @Column(name = "para_object_brand_id")
    String paraObjectBrandId;

    @Column(name = "para_object_type_id")
    String paraObjectTypeId;

    @Column(name = "para_object_model_id")
    String paraObjectModelId;

    @Column(name = "para_prod_matrix_id")
    String paraProductMatrixId;

    @Column(name = "para_fin_type_id")
    Long paraFinTypeId;

    @Column(name = "para_payment_type_id")
    String paraPaymentTypeId;

    @Column(name = "para_dp_net_rate_min")
    Long paraDpNetRateMin;

    @Column(name = "para_dp_net_rate_max")
    Long paraDpNetRateMax;

    @Column(name = "para_max_tenor")
    BigDecimal paraMaxTenor;

    @Column(name = "para_min_tenor")
    BigDecimal paraMinTenor;

    @Column(name = "para_landing_rate")
    BigDecimal paraLandingRate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @Column(name = "para_upld_lr_status_cd")
    String paraUploadLrStatusCd;

    @Column(name = "para_prod_program_id")
    String paraProductProgramId;

    @Column(name = "para_installment_type_id")
    String paraInstallmentTypeId;

}
