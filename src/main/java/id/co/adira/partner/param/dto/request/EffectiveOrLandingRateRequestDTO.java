package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EffectiveOrLandingRateRequestDTO {

    private String programId;

    private String objectModelId;

    private String objectTypeId;

    private BigDecimal tenor;

    private String pic;

    private String objectId;
}
