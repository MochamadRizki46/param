package id.co.adira.partner.param.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MarketPriceResponseDTO {
    private String objectTypeName;

    private String objectBrandName;

    private String objectGroupName;

    private String objectModelName;

    private BigDecimal manufacturerYear;

    private BigDecimal marketPriceValue;
}
