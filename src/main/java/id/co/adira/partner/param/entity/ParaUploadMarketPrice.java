package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "para_upld_marketprice")
@Data
public class ParaUploadMarketPrice {

    @Id
    @Column(name = "para_cp_sentra_id")
    String paraCpSentraId;

    @Column(name = "para_cp_sentra_name")
    String paraCpSentraName;

    @Column(name = "para_object_type_id")
    String paraObjectTypeId;

    @Column(name = "para_object_type_name")
    String paraObjectTypeName;

    @Column(name = "para_object_brand_id")
    String paraObjectBrandId;

    @Column(name = "para_object_brand_name")
    String paraObjectBrandName;

    @Column(name = "para_object_group_id")
    String paraObjectGroupId;

    @Column(name = "para_object_group_name")
    String paraObjectGroupName;

    @Column(name = "para_object_model_id")
    String paraObjectModelId;

    @Column(name = "para_object_model_name")
    String paraObjectModelName;

    @Column(name = "para_mfg_year")
    BigDecimal paraMfgYear;

    @Column(name = "para_marketprice")
    BigDecimal paraMarketPrice;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;
}
