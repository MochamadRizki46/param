package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TipeUnitRequestDTO {

    private String objectBrandName;

    @NotEmpty(message = "Object Group Id can't be empty!")
    private String objectGroupId;
}
