package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.ParaObjectResponseDTO;
import id.co.adira.partner.param.entity.ParaObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaObjectRepository extends JpaRepository<ParaObject, String> {
    @Query("select new id.co.adira.partner.param.dto.response.ParaObjectResponseDTO(a.paraObjectId, b.paraObjectGroupId) from ParaObject a join ParaObjectGroup b on " +
            "a.paraObjectGroup.paraObjectGroupId = b.paraObjectGroupId " +
            "where a.paraObjectId = :objectCode")
    ParaObjectResponseDTO findParaObjectAndGroupBasedOnObjectCode(String objectCode);
}
