package id.co.adira.partner.param.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class BiayaAdminRequestDTO {

    @NotEmpty(message = "Program Id can't be empty!")
    String programId;

    @NotEmpty(message = "Object Model Id Can't Be Empty!")
    String objectModelId;

    @NotEmpty(message = "Tenor can't be empty!")
    String tenor;

    @NotEmpty(message = "Object Code can't be empty!")
    String objectCode;

    @NotNull(message = "OTR can't be empty!")
    BigDecimal otr;
}
