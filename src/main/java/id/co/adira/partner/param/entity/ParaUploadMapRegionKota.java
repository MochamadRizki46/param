package id.co.adira.partner.param.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_upld_map_region_kota")
public class ParaUploadMapRegionKota {
    @Id
    @Column(name = "para_upld_map_region_kota_id")
    String paraUploadMapRegionKotaId;

    @Column(name = "para_cp_area_id")
    String paraCpAreaId;

    @Column(name = "para_kabkot_id")
    String paraKabKotId;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Double logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;
}
