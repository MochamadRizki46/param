package id.co.adira.partner.param.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipeUnitDetailResponseDTO {
    private String objectTypeId;

    private String objectGenreId;

    private String objectBrandName;

    private Date effectiveDate;
}
