package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OtrRequestDTO {

    private String objectModelId;
    private String tipeKendaraan;
    private BigDecimal manufacturerYear;
    private String kabkotId;
}
