package id.co.adira.partner.param.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KabKotDetailResponseDTO {
    @JsonProperty("PARENT_KODE")
    String provinsiCode;

    @JsonProperty("KODE")
    String code;

    @JsonProperty("DESKRIPSI")
    String description;
}
