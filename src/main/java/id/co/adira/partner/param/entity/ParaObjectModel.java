package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_object_model")
@Data
public class ParaObjectModel {

    @Id
    @Column(name = "para_object_model_id")
    String paraObjectModelId;

    @Column(name = "para_object_type_id")
    String paraObjectTypeId;

    @Column(name = "para_object_genre_id")
    String paraObjectGenreId;

    @Column(name = "para_object_model_name")
    String paraObjectModelName;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "para_object_brand_id")
    ParaObjectBrand paraObjectBrand;
}
