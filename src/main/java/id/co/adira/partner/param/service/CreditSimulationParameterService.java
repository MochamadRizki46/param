package id.co.adira.partner.param.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.adira.partner.param.dto.request.*;
import id.co.adira.partner.param.dto.response.*;
import id.co.adira.partner.param.entity.ParaProductProgram;
import id.co.adira.partner.param.entity.ParaUploadLtv;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectBrandRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectGroupRepository;
import id.co.adira.partner.param.repository.credsimparamrepo.*;
import id.co.adira.partner.param.util.ObjectConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static id.co.adira.partner.param.constant.AppEnum.*;
import static id.co.adira.partner.param.constant.LoggerEnum.*;

@Service
public class CreditSimulationParameterService {

    private final ParaUploadAdminFeeRepository paraUploadAdminFeeRepository;
    private final ParaUploadLandingRateRepository paraUploadLandingRateRepository;
    private final ParaProductProgramRepository paraProductProgramRepository;
    private final ParaInstallmentTypeRepository paraInstallmentTypeRepository;
    private final ParaUploadProfitFeeRepository paraUploadProfitFeeRepository;
    private final ParaUploadMarketPriceRepository paraUploadMarketPriceRepository;
    private final ParaObjectGroupRepository paraObjectGroupRepository;
    private final ParaObjectBrandRepository paraObjectBrandRepository;
    private final ParaUploadLtvRepository paraUploadLtvRepository;
    private final ObjectConverter objectConverter;

    @Autowired
    public CreditSimulationParameterService(ParaUploadAdminFeeRepository paraUploadAdminFeeRepository,
                                            ParaUploadLandingRateRepository paraUploadLandingRateRepository,
                                            ParaProductProgramRepository paraProductProgramRepository,
                                            ParaUploadProfitFeeRepository paraUploadProfitFeeRepository,
                                            ParaUploadMarketPriceRepository paraUploadMarketPriceRepository,
                                            ParaObjectGroupRepository paraObjectGroupRepository,
                                            ParaObjectBrandRepository paraObjectBrandRepository,
                                            ParaUploadLtvRepository paraUploadLtvRepository,
                                            ParaInstallmentTypeRepository paraInstallmentTypeRepository,
                                            ObjectConverter objectConverter) {
        this.paraUploadAdminFeeRepository = paraUploadAdminFeeRepository;
        this.paraUploadLandingRateRepository = paraUploadLandingRateRepository;
        this.paraProductProgramRepository = paraProductProgramRepository;
        this.paraUploadProfitFeeRepository = paraUploadProfitFeeRepository;
        this.paraUploadMarketPriceRepository = paraUploadMarketPriceRepository;
        this.paraObjectGroupRepository = paraObjectGroupRepository;
        this.paraObjectBrandRepository = paraObjectBrandRepository;
        this.paraUploadLtvRepository = paraUploadLtvRepository;
        this.paraInstallmentTypeRepository = paraInstallmentTypeRepository;
        this.objectConverter = objectConverter;
    }

    public static final Logger LOGGER = LoggerFactory.getLogger(CreditSimulationParameterService.class);


    public BaseResponse getBiayaAdmin(BiayaAdminRequestDTO biayaAdminRequestDTO) throws JsonProcessingException {
        BaseResponse baseResponse = new BaseResponse();

        mapBiayaAdmin(baseResponse, biayaAdminRequestDTO);
        return baseResponse;
    }

    private void mapBiayaAdmin(BaseResponse baseResponse, BiayaAdminRequestDTO biayaAdminRequestDTO) throws JsonProcessingException {

        try {
            insertBiayaAdminToResponse(baseResponse, biayaAdminRequestDTO.getObjectModelId(), biayaAdminRequestDTO.getProgramId(), biayaAdminRequestDTO);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_BIAYA_ADMIN.getMessage());
        }
    }

    private void insertBiayaAdminToResponse(BaseResponse baseResponse, String paraObjectModelId, String paraProductProgramId, BiayaAdminRequestDTO biayaAdminRequestDTO) throws JsonProcessingException {
        String objectGroupId = paraObjectGroupRepository.getParaObjectGroupByParaObjectId(biayaAdminRequestDTO.getObjectCode());

        baseResponse.setStatus(HttpStatus.OK);
        if (objectGroupId.equals("")) {
            baseResponse.setMessage(OBJECT_GROUP_NOT_FOUND.getMessage());
            baseResponse.setData(setBiayaAdminResponseToDefaultValue());
        } else {
            BigDecimal tenorInBigDecimal = new BigDecimal(biayaAdminRequestDTO.getTenor());
            if(!objectGroupId.equals("003"))
                insertBiayaAdminForAutomotive(baseResponse, objectGroupId, tenorInBigDecimal, paraObjectModelId, paraProductProgramId, biayaAdminRequestDTO);

            else
                insertBiayaAdminForDurable(baseResponse, tenorInBigDecimal, paraObjectModelId, paraProductProgramId);
        }
        LOGGER.info(SUCCESS_GET_BIAYA_ADMIN.getMessage(), objectConverter.convertObjectToString(baseResponse));
    }

    private void insertBiayaAdminForDurable(BaseResponse baseResponse, BigDecimal tenorInBigDecimal,
                                            String paraObjectModelId, String paraProductProgramId) {

        BiayaAdminResponseDTO biayaAdminResponseDTO = paraUploadProfitFeeRepository.
                findBiayaAdminCashAndCreditForDurable(paraProductProgramId,
                        paraObjectModelId, tenorInBigDecimal);

        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(biayaAdminResponseDTO == null ? setBiayaAdminResponseToDefaultValue() : biayaAdminResponseDTO);
    }

    private void insertBiayaAdminForAutomotive(BaseResponse baseResponse, String objectGroupId, BigDecimal tenorInBigDecimal,
                                               String paraObjectModelId, String paraProductProgramId,
                                               BiayaAdminRequestDTO biayaAdminRequestDTO) {
        BiayaAdminResponseDTO biayaAdminResponseDTO = paraUploadProfitFeeRepository.
                findBiayaAdminCashAndCreditForAutomotive(paraProductProgramId,
                        paraObjectModelId, tenorInBigDecimal, biayaAdminRequestDTO.getObjectCode());

        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(biayaAdminResponseDTO == null ? setBiayaAdminResponseToDefaultValue() : biayaAdminResponseDTO);
    }

    private Object setBiayaAdminResponseToDefaultValue() {
        return new BiayaAdminResponseDTO(BigDecimal.ZERO, BigDecimal.ZERO);
    }

    public BaseResponse getEffectiveOrLandingRate(EffectiveOrLandingRateRequestDTO effectiveOrLandingRateRequestDTO) {
        BaseResponse baseResponse = new BaseResponse();

        mapEffectiveOrLandingRate(baseResponse, effectiveOrLandingRateRequestDTO);
        return baseResponse;
    }

    public BaseResponse getInsuranceRateByProgramId(String programId) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            mapInsuranceRate(baseResponse, programId);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_INSURANCE_RATE.getMessage());
        }
        return baseResponse;
    }

    public BaseResponse getDataProgramByProgramId(String programId) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            mapDataProgramByProgramId(baseResponse, programId);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_DATA_PROGRAM.getMessage());
        }
        return baseResponse;
    }

    public BaseResponse getAllDataProgram() {
        BaseResponse baseResponse = new BaseResponse();
        try {
            mapAllDataProgram(baseResponse);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_DATA_PROGRAM.getMessage());
        }
        return baseResponse;
    }

    public BaseResponse getInstallmentType() {
        BaseResponse baseResponse = new BaseResponse();
        try {
            mapInstallmentType(baseResponse);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_INSTALLMENT_TYPE.getMessage());
        }
        return baseResponse;
    }

    private void mapAllDataProgram(BaseResponse baseResponse) {
        List<ParaProductProgram> paraProductPrograms = paraProductProgramRepository.findAll();
        ParaProgramListResponseDTO programListResponseDTO = new ParaProgramListResponseDTO();
        List<ParaProgramResponseDTO> paraProgramResponseDTOS = new ArrayList<>();

        for (ParaProductProgram productProgram : paraProductPrograms) {
            ParaProgramResponseDTO paraProgramResponseDTO = new ParaProgramResponseDTO();

            paraProgramResponseDTO.setParaProdProgramId(productProgram.getParaProductProgramId());
            paraProgramResponseDTO.setParaProdProgramName(productProgram.getParaProductProgramName());
            paraProgramResponseDTO.setEffectiveDate(productProgram.getEffectiveDate().toString());

            paraProgramResponseDTOS.add(paraProgramResponseDTO);
        }
        programListResponseDTO.setPrograms(paraProgramResponseDTOS);

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(paraProgramResponseDTOS);
    }

    private void mapInstallmentType(BaseResponse baseResponse) {
        List<ParaInstallmentTypeResponseDTO> paraInstallmentTypeResponseDTO = new ArrayList<>();


        paraInstallmentTypeResponseDTO = paraInstallmentTypeRepository.findParaInstallmentType();

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(paraInstallmentTypeResponseDTO);
    }

    private void mapDataProgramByProgramId(BaseResponse baseResponse, String programId) {
        ParaProductProgram paraProductProgram = paraProductProgramRepository.getParaProductProgramByParaProductProgramId(programId);
        ParaProgramResponseDTO paraProgramResponseDTO = new ParaProgramResponseDTO();

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(paraProductProgram != null ? insertProductProgramIntoResponse(baseResponse, paraProductProgram, paraProgramResponseDTO) : insertProductProgramToDefaultValue(paraProgramResponseDTO, programId));
    }

    private Object insertProductProgramToDefaultValue(ParaProgramResponseDTO paraProgramResponseDTO, String programId) {
        paraProgramResponseDTO.setParaProdProgramId(programId);
        paraProgramResponseDTO.setParaProdProgramName("");
        paraProgramResponseDTO.setEffectiveDate("");

        return paraProgramResponseDTO;
    }

    private Object insertProductProgramIntoResponse(BaseResponse baseResponse, ParaProductProgram paraProductProgram, ParaProgramResponseDTO paraProgramResponseDTO) {
        paraProgramResponseDTO.setParaProdProgramId(paraProductProgram.getParaProductProgramId());
        paraProgramResponseDTO.setParaProdProgramName(paraProductProgram.getParaProductProgramName());
        paraProgramResponseDTO.setEffectiveDate(paraProductProgram.getEffectiveDate().toString());

        return paraProgramResponseDTO;
    }

    private void mapInsuranceRate(BaseResponse baseResponse, String programId) throws JsonProcessingException {
        InsuranceRateResponseDTO insuranceRateResponseDTO = paraUploadAdminFeeRepository.getInsuranceRateByProgramId(programId);
        insertInsuranceRateToResponse(baseResponse, programId, insuranceRateResponseDTO);
    }

    private void insertInsuranceRateToResponse(BaseResponse baseResponse, String programId, InsuranceRateResponseDTO insuranceRateResponseDTO) throws JsonProcessingException {
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(insuranceRateResponseDTO != null ? insuranceRateResponseDTO : insertInsuranceRateResponseToDefaultValue(programId));

        LOGGER.info(SUCCESS_GET_INSURANCE_RATE.getMessage(), objectConverter.convertObjectToString(baseResponse));
    }

    private Object insertInsuranceRateResponseToDefaultValue(String programId) {
        InsuranceRateResponseDTO insuranceRateResponseDTO = new InsuranceRateResponseDTO();
        insuranceRateResponseDTO.setProgramId(programId);
        insuranceRateResponseDTO.setInsuranceRate((double) 0L);

        return insuranceRateResponseDTO;
    }

    private void mapEffectiveOrLandingRate(BaseResponse baseResponse, EffectiveOrLandingRateRequestDTO effectiveOrLandingRateRequestDTO) {
        try {
            insertEffectiveOrLandingRateToResponse(baseResponse, effectiveOrLandingRateRequestDTO);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_EFFECTIVE_OR_LANDING_RATE.getMessage());
        }
    }

    private void insertEffectiveOrLandingRateToResponse(BaseResponse baseResponse, EffectiveOrLandingRateRequestDTO effectiveOrLandingRateRequestDTO) throws JsonProcessingException {
        baseResponse.setStatus(HttpStatus.OK);
        String objectBrandId = paraObjectBrandRepository.getParaObjectBrandIdByObjectModelId(effectiveOrLandingRateRequestDTO.getObjectModelId());

        EffectiveOrLandingRateResponseDTO effectiveOrLandingRateResponseDTO = new EffectiveOrLandingRateResponseDTO();

        if (objectBrandId == null || objectBrandId.equals("")) {
            baseResponse.setMessage(OBJECT_BRAND_NOT_FOUND.getMessage());
            baseResponse.setData(insertEffectiveOrLandingRateWithDefaultValue());
        } else {
            effectiveOrLandingRateResponseDTO = paraUploadLandingRateRepository.
                    findEffectiveOrLandingRate(effectiveOrLandingRateRequestDTO.getObjectModelId(),
                            effectiveOrLandingRateRequestDTO.getTenor(), effectiveOrLandingRateRequestDTO.getPic(),
                            effectiveOrLandingRateRequestDTO.getObjectId(), effectiveOrLandingRateRequestDTO.getObjectTypeId(),
                            objectBrandId, effectiveOrLandingRateRequestDTO.getProgramId());

            if (effectiveOrLandingRateResponseDTO == null)
                effectiveOrLandingRateResponseDTO = searchEffectiveOrLandingRateWithAllParam(effectiveOrLandingRateRequestDTO.getPic(),
                        effectiveOrLandingRateRequestDTO.getObjectId(), effectiveOrLandingRateRequestDTO.getTenor());

            baseResponse.setMessage(SUCCESS.getMessage());
            baseResponse.setData(effectiveOrLandingRateResponseDTO);
        }
        LOGGER.info(SUCCESS_GET_EFFECTIVE_OR_LANDING_RATE.getMessage(), objectConverter.convertObjectToString(baseResponse));
    }

    private EffectiveOrLandingRateResponseDTO searchEffectiveOrLandingRateWithAllParam(String pic, String objectId, BigDecimal tenor) {
        EffectiveOrLandingRateResTempDTO effectiveOrLandingRateResTempDTO = paraUploadLandingRateRepository.findEffectiveOrLandingRateWithAllParam(pic, objectId, tenor).get(0);
        return new EffectiveOrLandingRateResponseDTO(effectiveOrLandingRateResTempDTO != null ? effectiveOrLandingRateResTempDTO.getEffectiveOrLandingRate() : BigDecimal.ZERO);
    }

    private Object insertEffectiveOrLandingRateWithDefaultValue() {
        return new EffectiveOrLandingRateResponseDTO(BigDecimal.ZERO);
    }

    private void insertErrorResponseWithException(BaseResponse baseResponse, Exception e, String logMessage) {
        baseResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        baseResponse.setMessage(FAILED.getMessage());
        baseResponse.setData(THERE_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());

        LOGGER.error(logMessage, e.getMessage());
    }

    public BaseResponse getMinMaxPencairan(MinMaxPencairanRequestDTO minMaxPencairanRequestDTO) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            mapMinMaxValuePencairan(baseResponse, minMaxPencairanRequestDTO);
        } catch (Exception e) {
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_MIN_MAX_VALUE_PENCAIRAN.getMessage());
        }
        return baseResponse;
    }

    private void mapMinMaxValuePencairan(BaseResponse baseResponse, MinMaxPencairanRequestDTO minMaxPencairanRequestDTO) throws JsonProcessingException {
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());

        insertMinMaxPencairanToResponse(baseResponse, minMaxPencairanRequestDTO, minMaxPencairanRequestDTO.getKabKotId());
    }


    private Object insertMinMaxPencairanToResponse(BaseResponse baseResponse, MinMaxPencairanRequestDTO minMaxPencairanRequestDTO, String kabKotId) {
        List<BatasAtasAndBatasBawahResponseDTO> batasAtasDatas = paraUploadMarketPriceRepository.getOtrFromLtv(minMaxPencairanRequestDTO.getManufacturerYear(), kabKotId, minMaxPencairanRequestDTO.getObjectModelId());
        BatasAtasAndBatasBawahResponseDTO batasAtasAndBatasBawahResponseDTO = new BatasAtasAndBatasBawahResponseDTO();

        if (batasAtasDatas.isEmpty()) {
            batasAtasAndBatasBawahResponseDTO.setBatasBawahRate(BigDecimal.ZERO);
            batasAtasAndBatasBawahResponseDTO.setBatasAtasRate(new BigDecimal(1));
            batasAtasAndBatasBawahResponseDTO.setOtr(BigDecimal.ZERO);
        } else {
            batasAtasAndBatasBawahResponseDTO = batasAtasDatas.get(0);
        }
        List<MinMaxPencairanResponseDTO> minMaxPencairanResponseDTOS = paraUploadMarketPriceRepository.getMinMaxFromMarketPlace(minMaxPencairanRequestDTO.getObjectModelId(),
                minMaxPencairanRequestDTO.getObjectBrandId(), minMaxPencairanRequestDTO.getTipeKendaraan().toLowerCase(), new BigDecimal(minMaxPencairanRequestDTO.getManufacturerYear()));

        MinMaxPencairanResponseDTO minMaxPencairanResponseDTO = minMaxPencairanResponseDTOS.get(0);

        if(minMaxPencairanResponseDTOS.isEmpty()) {
            baseResponse.setData(new MinMaxPencairanResponseDTO());
            baseResponse.setMessage(DATA_MARKETPRICE_NOT_FOUND.getMessage());
        }
        else
            baseResponse.setData(insertMinMaxPencairanValueWhenExists(minMaxPencairanResponseDTO, minMaxPencairanRequestDTO, batasAtasAndBatasBawahResponseDTO));

        return baseResponse;
    }

    private Object insertMinMaxPencairanValueWhenExists(MinMaxPencairanResponseDTO minMaxPencairanResponseDTO, MinMaxPencairanRequestDTO minMaxPencairanRequestDTO, BatasAtasAndBatasBawahResponseDTO batasAtasAndBatasBawahResponseDTO) {
        minMaxPencairanResponseDTO.setMinValuePencairan(BigDecimal.ZERO);

        minMaxPencairanResponseDTO.setMaxValuePencairan(batasAtasAndBatasBawahResponseDTO.getBatasAtasRate().compareTo(new BigDecimal(1)) != 0 ?
                batasAtasAndBatasBawahResponseDTO.getBatasAtasRate().divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP).multiply(minMaxPencairanResponseDTO.getMaxValuePencairan())
                :
                minMaxPencairanResponseDTO.getMaxValuePencairan());

        return minMaxPencairanResponseDTO;
    }

    public BaseResponse calculateCicilan(CalculateCicilanRequestDTO calculateCicilanRequestDTO) throws JsonProcessingException {
        BaseResponse baseResponse = new BaseResponse();

        if(calculateCicilanRequestDTO.getTenor() <= 0)
            insertErrorResponseAboutCalculateCicilan(baseResponse, calculateCicilanRequestDTO.getTenor());
        else
            insertCalculateCicilanToResponse(baseResponse, calculateCicilanRequestDTO);

        return baseResponse;
    }

    private void insertCalculateCicilanToResponse(BaseResponse baseResponse, CalculateCicilanRequestDTO calculateCicilanRequestDTO) throws JsonProcessingException {

        CalculateCicilanResponseDTO calculateCicilanResponseDTO = new CalculateCicilanResponseDTO();
        calculateCicilanResponseDTO.setValuePencairan(calculateCicilanRequestDTO.getValuePencairan());
        calculateCicilanResponseDTO.setTenor(calculateCicilanRequestDTO.getTenor());
        calculateCicilanResponseDTO.setInstalment(calculateCicilanRequestDTO.getValuePencairan().divide(new BigDecimal(calculateCicilanRequestDTO.getTenor()), 2, RoundingMode.HALF_UP));

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(calculateCicilanResponseDTO);

        LOGGER.info(SUCCESS_CALCULATE_CICILAN.getMessage(), objectConverter.convertObjectToString(baseResponse));
    }

    private void insertErrorResponseAboutCalculateCicilan(BaseResponse baseResponse, Integer tenor) {
        baseResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        baseResponse.setMessage(FAILED.getMessage());
        baseResponse.setData(TENOR_INVALID.getMessage() + tenor);

        LOGGER.error(ERROR_WHILE_CALCULATE_CICILAN.getMessage(), TENOR_INVALID.getMessage() + tenor);
    }

    public BaseResponse getAsuransiCashAndCredit(AsuransiCashAndCreditRequestDTO asuransiCashAndCreditRequestDTO) {
        BaseResponse baseResponse = new BaseResponse();
        try{
            mapAsuransiCashAndCredit(baseResponse, asuransiCashAndCreditRequestDTO);
        }catch(Exception e){
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_ASURANSI_CASH_AND_CREDIT.getMessage());
        }
        return baseResponse;
    }

    private void mapAsuransiCashAndCredit(BaseResponse baseResponse, AsuransiCashAndCreditRequestDTO asuransiCashAndCreditRequestDTO) throws JsonProcessingException {
        String objectGroupId = paraObjectGroupRepository.getParaObjectGroupByParaObjectId(asuransiCashAndCreditRequestDTO.getObjectCode());
        baseResponse.setStatus(HttpStatus.OK);

        if (objectGroupId == null || objectGroupId.equals("")) {
            baseResponse.setMessage(OBJECT_GROUP_NOT_FOUND.getMessage());
            baseResponse.setData(new AsuransiCashAndCreditResponseDTO());
        } else {
            List<AsuransiCashAndCreditResponseDTO> asuransiCashAndCreditResponseDTOS = paraUploadProfitFeeRepository.findAsuransiCashAndCreditRate(asuransiCashAndCreditRequestDTO.getProgramId(),
                    asuransiCashAndCreditRequestDTO.getObjectModelId(),
                    new BigDecimal(asuransiCashAndCreditRequestDTO.getTenor()),
                    asuransiCashAndCreditRequestDTO.getObjectCode());

            baseResponse.setMessage(SUCCESS.getMessage());
            baseResponse.setData(calculateAsuransiCashAndCredit(asuransiCashAndCreditResponseDTOS, asuransiCashAndCreditRequestDTO.getOtr()));
        }
        LOGGER.info(SUCCESS_GET_ASURANSI_CASH_AND_CREDIT.getMessage(), objectConverter.convertObjectToString(baseResponse));
    }

    private Object calculateAsuransiCashAndCredit(List<AsuransiCashAndCreditResponseDTO> asuransiCashAndCreditResponseDTOS, Double otr) {
        BigDecimal asuransiCashSum = BigDecimal.ZERO;
        BigDecimal asuransiCreditSum = BigDecimal.ZERO;

        for(AsuransiCashAndCreditResponseDTO currentAsuransiCashAndCreditRate : asuransiCashAndCreditResponseDTOS){
            asuransiCashSum = asuransiCashSum.add(currentAsuransiCashAndCreditRate.getAsuransiCashAmt());
            asuransiCreditSum = asuransiCreditSum.add(currentAsuransiCashAndCreditRate.getAsuransiCreditAmt());
        }
        return new AsuransiCashAndCreditResponseDTO(asuransiCashSum.divide(BigDecimal.valueOf(otr)), asuransiCreditSum.divide(BigDecimal.valueOf(otr)));
    }

    public BaseResponse getDpValueForDatun(DpValueForDanaTunaiRequestDTO otrRequestDTO) {
        BaseResponse baseResponse = new BaseResponse();

        try{
            mapGetDpValue(baseResponse, otrRequestDTO);
        }catch(Exception e){
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_OTR.getMessage());
        }
        return baseResponse;
    }


    private void mapGetDpValue(BaseResponse baseResponse, DpValueForDanaTunaiRequestDTO otrRequestDTO) throws JsonProcessingException {
        baseResponse.setStatus(HttpStatus.OK);

        List<MarketPriceResponseDTO> marketPriceResponseDTOS = paraUploadMarketPriceRepository.getOtr(otrRequestDTO.getObjectGroupId(), otrRequestDTO.getObjectModelId(), otrRequestDTO.getObjectTypeId(), otrRequestDTO.getObjectBrandId(), otrRequestDTO.getManufacturerYear());
        if(marketPriceResponseDTOS.isEmpty()) {
            baseResponse.setMessage(OTR_NOT_FOUND.getMessage());
            baseResponse.setData(null);
        }else
            mapDpValueToResponse(baseResponse, marketPriceResponseDTOS.get(0), otrRequestDTO);

        LOGGER.info(SUCCESS_GET_DP_VALUE_FOR_DATUN.getMessage(), objectConverter.convertObjectToString(baseResponse));
    }

    private void mapDpValueToResponse(BaseResponse baseResponse, MarketPriceResponseDTO marketPriceResponseDTO, DpValueForDanaTunaiRequestDTO otrRequestDTO) {
        DpValueForDatunResponseDTO dpValueForDatunResponseDTO = new DpValueForDatunResponseDTO(
                marketPriceResponseDTO.getMarketPriceValue(),
                marketPriceResponseDTO.getMarketPriceValue().subtract(otrRequestDTO.getDisbursementAmt())
        );

        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(dpValueForDatunResponseDTO);
    }

    public BaseResponse getOtrFromLtv(OtrRequestDTO otrRequestDTO) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());

        try{
            mapGetOtr(baseResponse, otrRequestDTO);
        } catch(Exception e){
            insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_OTR.getMessage());
        }

        return baseResponse;
    }

    private void mapGetOtr(BaseResponse baseResponse, OtrRequestDTO otrRequestDTO) throws JsonProcessingException {
        List<OtrResponseDTO> otrResponses = paraUploadLtvRepository.findAllOtr(otrRequestDTO.getObjectModelId(), String.valueOf(otrRequestDTO.getManufacturerYear()));
        if(otrResponses.isEmpty()){
            baseResponse.setMessage(OTR_NOT_FOUND.getMessage());
            baseResponse.setData(null);

        } else{
            baseResponse.setMessage(SUCCESS.getMessage());
            baseResponse.setData(otrResponses.get(0));

            LOGGER.info(SUCCESS_GET_OTR_VALUE_FROM_LTV.getMessage(), objectConverter.convertObjectToString(baseResponse));
        }
    }
}
