package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.entity.ParaUploadProfit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaUploadProfitRepository extends JpaRepository<ParaUploadProfit, String> {

}
