package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_prod_matrix_brand")
@Data
public class ParaProductMatrixBrand {

    @Id
    @Column(name = "para_prod_matrix_brand_id")
    String paraProductMatrixBrandId;

    @Column(name = "para_prod_matrix_brand_desc")
    String paraProductMatrixBrandDesc;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "para_prod_sales_group_id")
    ParaProductSalesGroup paraProductSalesGroup;

    @ManyToOne
    @JoinColumn(name = "para_prod_matrix_id")
    ParaProductMatrix paraProductMatrix;


}
