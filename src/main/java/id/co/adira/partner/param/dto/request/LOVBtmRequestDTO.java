package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LOVBtmRequestDTO {
    private String brandName;

    @NotEmpty(message = "objectId can't be empty!")
    private String objectId;
}
