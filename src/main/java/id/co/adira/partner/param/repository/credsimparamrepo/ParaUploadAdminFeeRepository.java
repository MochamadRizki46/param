package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.InsuranceRateResponseDTO;
import id.co.adira.partner.param.entity.ParaUploadAdminFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaUploadAdminFeeRepository extends JpaRepository<ParaUploadAdminFee, String> {

    @Query("select new id.co.adira.partner.param.dto.response.InsuranceRateResponseDTO(pulr.paraProductProgramId, pupd.paraInsrCoc) " +
            "from ParaUploadLandingRate pulr inner join ParaUploadProfit pup on " +
            "pulr.paraProductProgramId = pup.paraProdProgramId join ParaUploadProfitDetail pupd " +
            "on pup.paraUploadProfitId = pupd.paraUploadProfit " +
            "where pup.paraProdProgramId = :programId")
    InsuranceRateResponseDTO getInsuranceRateByProgramId(@Param(value = "programId") String programId);



}
