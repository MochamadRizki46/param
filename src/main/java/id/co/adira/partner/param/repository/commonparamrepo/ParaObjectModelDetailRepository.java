package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.dto.response.ErpBpDealerResponseDTO;
import id.co.adira.partner.param.dto.response.LovBrandTipeModelDetailResponseDTO;
import id.co.adira.partner.param.dto.response.ObjectModelDetailDetailResponseDTO;
import id.co.adira.partner.param.dto.response.ObjectModelDetailDurableResponseDTO;
import id.co.adira.partner.param.entity.ParaObjectModelDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaObjectModelDetailRepository extends JpaRepository<ParaObjectModelDetail, String> {
    @Query("select new id.co.adira.partner.param.dto.response.LovBrandTipeModelDetailResponseDTO(pob.paraObjectBrandId, pob.paraObjectBrandName, pot.paraObjectTypeId, pot.paraObjectTypeName, pom.paraObjectModelId, pom.paraObjectModelName, po.paraObjectId, po.paraObjectName) " +
            "from ParaObjectModel pom inner join ParaObjectType pot on " +
            "pom.paraObjectTypeId = pot.paraObjectTypeId left join ParaObjectBrand pob on " +
            "pob.paraObjectBrandId = pom.paraObjectBrand " +
            "join ParaObjectGroup pog on pog.paraObjectGroupId = pob.paraObjectGroup " +
            "join ParaObject po on po.paraObjectGroup = pog.paraObjectGroupId " +
            "and po.paraObjectId = :objectId " +
            "where upper(pob.paraObjectBrandName) like %:brandName% or " +
            "upper(pot.paraObjectTypeName) like %:brandName% or " +
            "upper(pom.paraObjectModelName) like %:brandName% " +
            "order by pob.paraObjectBrandName, pot.paraObjectTypeName, pom.paraObjectModelName asc")
    List<LovBrandTipeModelDetailResponseDTO> getLOVBrandTipeModelByObjectIdAndBrandName(String objectId, String brandName);

    @Query("select new id.co.adira.partner.param.dto.response.ObjectModelDetailDurableResponseDTO(pomd.paraObjectModelDetailId, pomd.paraObjectModelId, pomd.paraObjectModelDetail, pomd.paraObjectModelDetailDesc) " +
            "from ParaObjectModelDetail pomd where pomd.paraObjectModelId = :objectModelId")
    List<ObjectModelDetailDurableResponseDTO> getModelDetailDurableOnly(String objectModelId);

    @Query("select distinct new id.co.adira.partner.param.dto.response.ObjectModelDetailDetailResponseDTO(pom.paraObjectModelId, pom.paraObjectModelName, pob.paraObjectBrandId, pob.paraObjectBrandName, pot.paraObjectTypeId, pot.paraObjectTypeName) " +
            "from ErpBpIndustry ebi join ErpBpRole ebr on " +
            "ebr.erpBpBpartner = ebi.erpBpBpartner " +
            "join ParaObject po on " +
            "po.paraObjectId = ebi.erpBpkeySystem and po.active=0 " +
            "join ParaObjectBrand pob on  " +
            "pob.paraObjectGroup = po.paraObjectGroup and pob.active=0 " +
            "join ParaObjectModel pom on " +
            "pom.paraObjectBrand = pob.paraObjectBrandId and pom.active=0 " +
            "join ParaObjectType pot on pom.paraObjectTypeId = pot.paraObjectTypeId and pot.active=0 " +
            "where ebr.erpBpPartnerExternal = :dlc " +
            "and ebr.erpBpRoleCategory = 'ZP0101' " +
            "and po.paraObjectId = :objectId " +
            "and (upper(pom.paraObjectModelName) like %:search% or upper(pob.paraObjectBrandName) like %:search%  or upper(pot.paraObjectTypeName) like %:search%)")
    List<ObjectModelDetailDetailResponseDTO> getModelDetailByObjectIdAndSearch(String objectId, String search, String dlc);
    
    @Query("select distinct new id.co.adira.partner.param.dto.response.ObjectModelDetailDetailResponseDTO(pom.paraObjectModelId, pom.paraObjectModelName, pob.paraObjectBrandId, pob.paraObjectBrandName, pot.paraObjectTypeId, pot.paraObjectTypeName) " +
            "from ErpBpIndustry ebi join ErpBpRole ebr on " +
            "ebr.erpBpBpartner = ebi.erpBpBpartner " +
            "join ParaObject po on " +
            "po.paraObjectId = ebi.erpBpkeySystem and po.active=0 " +
            "join ParaObjectBrand pob on  " +
            "pob.paraObjectGroup = po.paraObjectGroup and pob.active=0 " +
            "join ParaObjectModel pom on " +
            "pom.paraObjectBrand = pob.paraObjectBrandId and pom.active=0 " +
            "join ParaObjectType pot on pom.paraObjectTypeId = pot.paraObjectTypeId and pot.active=0 " +
            "where ebr.erpBpPartnerExternal = :dlc " +
            "and ebr.erpBpRoleCategory = 'ZP0101' " +
            "and po.paraObjectId = :objectId " +
            "and upper(pob.paraObjectBrandName) = :brandCode "+
            "and (upper(pom.paraObjectModelName) like %:search% or upper(pot.paraObjectTypeName) like %:search%)")
    List<ObjectModelDetailDetailResponseDTO> getModelDetailByObjectIdAndBrand(String objectId, String search, String dlc, @Param("brandCode") String branCode);
    
   

    @Query("select distinct new id.co.adira.partner.param.dto.response.ErpBpDealerResponseDTO(ebi.erpBpkeySystem, ebr.erpBpPartnerExternal, ebi.erpBpIndSector) " +
            "from ErpBpIndustry ebi join ErpBpRole ebr on " +
            "ebi.erpBpBpartner = ebr.erpBpBpartner " +
            "where ebr.erpBpPartnerExternal = :dlc ")
    List<ErpBpDealerResponseDTO> getDealerCodeFromErp(String dlc);

}
