package id.co.adira.partner.param.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParaKabKotTempResponseDTO {
    private String kabKotId;

    private String kabKotName;
}
