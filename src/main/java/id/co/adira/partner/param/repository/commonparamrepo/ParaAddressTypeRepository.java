package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaAddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaAddressTypeRepository extends JpaRepository<ParaAddressType, String> {
}
