package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.ObjectModelDetailDetailResponseDTO;
import id.co.adira.partner.param.dto.response.TipeUnitDetailResponseDTO;
import id.co.adira.partner.param.entity.ParaObjectModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaObjectModelRepository extends JpaRepository<ParaObjectModel, String> {

    @Query("select new id.co.adira.partner.param.dto.response.TipeUnitDetailResponseDTO(pom.paraObjectTypeId, pom.paraObjectGenreId, pom.paraObjectModelName, pob.effectiveDate) " +
            "from ParaObjectModel pom join ParaObjectBrand pob on pom.paraObjectBrand = pob.paraObjectBrandId " +
            "where lower(pob.paraObjectBrandName) = lower(:objectBrandName) and " +
            "pob.paraObjectGroup.paraObjectGroupId = :objectGroupId")
    List<TipeUnitDetailResponseDTO> findTipeUnitByBrandNameAndObjectCode(@Param(value = "objectBrandName") String objectBrandName, @Param(value = "objectGroupId") String objectGroupId);

    @Query("select distinct new id.co.adira.partner.param.dto.response.ObjectModelDetailDetailResponseDTO(pom.paraObjectModelId, pom.paraObjectModelName, " +
            "pob.paraObjectBrandId, pob.paraObjectBrandName, pot.paraObjectTypeId, pot.paraObjectTypeName) " +
            "from ParaObjectModel pom join ParaObjectBrand pob on " +
            "pom.paraObjectBrand = pob.paraObjectBrandId and pob.active = 0" +
            "join ParaObjectGroup pog on " +
            "pob.paraObjectGroup = pog.paraObjectGroupId and pog.active = 0" +
            "join ParaObjectType pot on " +
            "pot.paraObjectTypeId = pom.paraObjectTypeId and pot.active = 0" +
            "join ParaObject po on " +
            "po.paraObjectGroup = pog.paraObjectGroupId and po.active = 0" +
            "where (upper(pom.paraObjectModelName) like %:search% or upper(pob.paraObjectBrandName) like %:search% )" +
            "and po.paraObjectId = :groupCode and pom.active = 0 " +
            "order by pob.paraObjectBrandId asc ")
    List<ObjectModelDetailDetailResponseDTO>  findObjectModelIfErpIsNullAndDlcLengthIsNotEqualsThanSix(String search, String groupCode);
}
