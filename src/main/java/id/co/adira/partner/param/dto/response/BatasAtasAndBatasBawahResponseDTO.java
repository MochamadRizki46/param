package id.co.adira.partner.param.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BatasAtasAndBatasBawahResponseDTO {

    BigDecimal batasBawahRate;

    BigDecimal batasAtasRate;

    BigDecimal otr;
}
