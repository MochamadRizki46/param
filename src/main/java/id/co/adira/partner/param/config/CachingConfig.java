/**
 * 
 */
package id.co.adira.partner.param.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


/**
 * @author Vionza
 *
 */

@Configuration
@EnableCaching
@EnableScheduling
public class CachingConfig {

	String[] cacheName = {"cacheParamPortofolio", "cacheParamModel", "cacheParamKelurahan", "cacheParamKabKot"};
	
    @Autowired
    CacheManager cacheManager;

    private static final Logger LOGGER = LoggerFactory.getLogger(CachingConfig.class);

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(cacheName);
    }

    @CacheEvict
    @Scheduled(fixedRateString = "${spring.boot.cache.param.refresh-time}")
    public void evictCache(){
    	for(String cache : cacheName) {
    		 cacheManager.getCache(cache);
    	}      
        LOGGER.info("Currently evict cache...");
    }

}
