package id.co.adira.partner.param.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MinMaxPencairanResponseDTO {
    String objectBrandName;

    String objectGroupName;

    String objectModelName;

    BigDecimal manufacturerYear;

    BigDecimal minValuePencairan;

    BigDecimal maxValuePencairan;
}
