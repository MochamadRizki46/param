package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "para_prod_matrix")
@Data
public class ParaProductMatrix {

    @Id
    @Column(name = "para_prod_matrix_id")
    String paraProdMatrixId;

    @Column(name = "para_prod_matrix_desc")
    String paraProdMatrixDesc;

    @Column(name = "effective_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date effectiveDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "para_object_id")
    ParaObject paraObject;

    @ManyToOne
    @JoinColumn(name = "para_prod_type_id")
    ParaProductType paraProductType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paraProductMatrix")
    List<ParaProductMatrixBrand> paraProductMatrixBrandList;

}
