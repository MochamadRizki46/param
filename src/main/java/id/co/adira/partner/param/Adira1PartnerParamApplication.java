package id.co.adira.partner.param;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Adira1PartnerParamApplication {

    public static void main(String[] args){
        SpringApplication.run(Adira1PartnerParamApplication.class);
    }
}
