package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.entity.ParaTenor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaTenorRepository extends JpaRepository<ParaTenor, String> {
	
    @Query(nativeQuery = true, value = "select max(cast(pt.para_tenor_name as int)) as para_tenor_name from para_tenor pt")
    int getMaxTenor();
}
