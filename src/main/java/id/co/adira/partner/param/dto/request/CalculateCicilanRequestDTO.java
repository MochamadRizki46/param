package id.co.adira.partner.param.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CalculateCicilanRequestDTO {
    private BigDecimal valuePencairan;

    @NotEmpty(message = "Tenor can't be empty!")
    private Integer tenor;
}
