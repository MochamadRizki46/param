package id.co.adira.partner.param.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_user_detail")
@Data
public class ParaUserDetail {

    @Id
    @Column(name = "id")
    Long id;

    @Column(name = "usertype")
    String userType;

    @Column(name = "userid")
    String userId;

    @Column(name = "roleid")
    String roleId;

    @Column(name = "placeofbirth")
    String birthPlace;

    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    Date birthDay;

    @Column(name = "email")
    String email;

    @Column(name = "handphone")
    String handphone;

    @Column(name = "wa")
    String waNumber;

    @Column(name = "partner_code")
    String partnerCode;

    @Column(name = "partner_name")
    String partnerName;

    @Column(name = "create_by")
    String createBy;

    @Column(name = "createdate")
    @Temporal(TemporalType.TIMESTAMP)
    Date createDate;

    @Column(name = "modify_by")
    String modifyBy;

    @Column(name = "modifydate")
    @Temporal(TemporalType.TIMESTAMP)
    Date modifyDate;

    @Column(name = "isactive")
    Integer isActive;

    @Column(name = "deleted")
    Integer deleted;

    @Column(name = "portfolio")
    String portfolio;

    @Column(name = "brand")
    String brand;

    @Column(name = "username")
    String username;
}
