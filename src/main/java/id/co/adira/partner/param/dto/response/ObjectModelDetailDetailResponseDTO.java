package id.co.adira.partner.param.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ObjectModelDetailDetailResponseDTO {

    @JsonProperty("OBMO_CODE")
    private String objectModelCode;

    @JsonProperty("OBMO_DESC")
    private String objectModelDesc;

    @JsonProperty("OBBR_CODE")
    private String objectBrandCode;

    @JsonProperty("OBBR_DESC")
    private String objectBrandDesc;

    @JsonProperty("OBTY_CODE")
    private String objectTypeCode;

    @JsonProperty("OBTY_DESC")
    private String objectTypeDesc;
}
