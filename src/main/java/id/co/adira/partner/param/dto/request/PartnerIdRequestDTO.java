package id.co.adira.partner.param.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class PartnerIdRequestDTO {
	@ApiModelProperty(position = 1)
	@NotEmpty(message = "search should not be null or empty")
	@Size(min = 3, message = "search should have at least 3 characters")
	String search;

	@ApiModelProperty(position = 2)
	@NotEmpty(message = "partner type should not be null or empty")
	@Size(min = 3, max = 3, message = "must have 3 characters")
	String partnerType;

}
