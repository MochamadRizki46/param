package id.co.adira.partner.param.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelRequestDTO {
    private String search;

    private String branchId;

    private String groupCode;

    private String dlc;

}
