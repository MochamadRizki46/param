package id.co.adira.partner.param.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ParamToBpEnum {
	DEALER("001","ZP0101"),
	MERCHANT("002","ZP0103"),//merchant offline
	AXI("008","ZP0107"),//axi perorangan
	KEDAY("009","ZP0109");

    private String partnerType;
    private String RoleCatBp;

}
