package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.dto.response.KabKotDetailResponseDTO;
import id.co.adira.partner.param.dto.response.KecamatanDetailResponseDTO;
import id.co.adira.partner.param.entity.ParaProvinsi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ParaProvinsiRepository extends JpaRepository<ParaProvinsi, String> {
    @Query("select new id.co.adira.partner.param.dto.response.KecamatanDetailResponseDTO(" +
            "pkl.paraKelurahanId, pkl.paraKelurahanName, pkc.paraKecamatanId, pkc.paraKecamatanName, pkk.paraKabKotId, pkk.paraKabKotName, pp.paraProvinsiId, pp.paraProvinsiName, pkl.paraZipCode) " +
            "from ParaProvinsi pp " +
            "inner join ParaKabKot pkk on "+
            "pp.paraProvinsiId = pkk.paraProvinsi and pp.active = 0" +
            "inner join ParaKecamatan pkc on " +
            "pkk.paraKabKotId = pkc.paraKabKot  and pkk.active = 0" +
            "inner join ParaKelurahan pkl on " +
            "pkc.paraKecamatanId = pkl.paraKecamatan and pkc.active = 0" +
            "where lower(pkl.paraKelurahanName) like %:kelurahanName% " +
            "and pkl.active = 0 " +
            "order by pp.paraProvinsiName, pkk.paraKabKotName, pkc.paraKecamatanName, pkl.paraKelurahanName asc")
    List<KecamatanDetailResponseDTO> getLocationDetailsByKelurahanName(@Param("kelurahanName") String kelurahanName);

    @Query("select distinct new id.co.adira.partner.param.dto.response.KabKotDetailResponseDTO(" +
            "pp.paraProvinsiId, pkk.paraKabKotId , pkk.paraKabKotName) " +
            "from ParaProvinsi pp " +
            "inner join ParaKabKot pkk on "+
            "pp.paraProvinsiId = pkk.paraProvinsi and pp.active = 0 " +
            "where lower(pkk.paraKabKotName) like %:kabKotName% " +
            "and pkk.active = 0 " +
            "order by pkk.paraKabKotName asc")
    List<KabKotDetailResponseDTO> getLocationDetailsKabKotByKabKotName(@Param("kabKotName") String kabKotName);
}
