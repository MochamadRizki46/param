package id.co.adira.partner.param.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_upld_adm_fee")
@Data
public class ParaUploadAdminFee {
    @Id
    @Column(name = "para_object_id")
    String paraObjectId;

    @Column(name = "para_object_type_id")
    String paraObjectTypeId;

    @Column(name = "para_object_brand_id")
    String paraObjectBrandId;

    @Column(name = "para_prod_matrix_id")
    String paraProductMatrixId;

    @Column(name = "para_fin_type_id")
    Double paraFinTypeId;

    @Column(name = "para_max_tenor")
    Double paraMaxTenor;

    @Column(name = "para_min_tenor")
    Double paraMinTenor;

    @Column(name = "para_max_otr")
    Double paraMaxOtr;

    @Column(name = "para_min_otr")
    Double paraMinOtr;

    @Column(name = "para_min_admin")
    Double paraMinAdmin;

    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @Column(name = "active")
    Integer active;

    @Column(name = "logid")
    Long logId;

    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_date")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date modifiedDate;

    @Column(name = "modified_by")
    String modifiedBy;
}
