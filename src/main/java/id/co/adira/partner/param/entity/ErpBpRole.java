package id.co.adira.partner.param.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "erp_bp_role")
@Data
public class ErpBpRole {
    @Id
    @Column(name = "erp_bp_bpartner")
    String erpBpBpartner;

    @Column(name = "erp_bp_rolecategory")
    String erpBpRoleCategory;

    @Column(name = "erp_bp_request_number")
    String erpBpRequestNumber;

    @Column(name = "erp_bp_partnerexternal")
    String erpBpPartnerExternal;

    @Column(name = "erp_bp_item_number")
    Integer erpBpItemNumber;

    @Column(name = "erp_bp_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    Date erpBpTimestamp;


}
