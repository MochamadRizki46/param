package id.co.adira.partner.param.repository.commonparamrepo;

import id.co.adira.partner.param.dto.response.ParaKabKotTempResponseDTO;
import id.co.adira.partner.param.entity.ParaKabKot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ParaKabKotRepository extends JpaRepository<ParaKabKot, String> {
    @Query("select new id.co.adira.partner.param.dto.response.ParaKabKotTempResponseDTO(a.paraKabKotId, a.paraKabKotName) from ParaKabKot a where a.paraKabKotId = :kabKotId and a.active = 0")
    ParaKabKotTempResponseDTO getParaKabKotByParaKabKotId(String kabKotId);

}
