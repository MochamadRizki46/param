package id.co.adira.partner.param.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import id.co.adira.partner.param.dto.request.LOVBtmRequestDTO;
import id.co.adira.partner.param.dto.request.ModelRequestDTO;
import id.co.adira.partner.param.dto.request.PartnerIdRequestDTO;
import id.co.adira.partner.param.dto.request.TipeUnitRequestDTO;
import id.co.adira.partner.param.dto.response.*;
import id.co.adira.partner.param.entity.ParaUserDetail;
import id.co.adira.partner.param.repository.commonparamrepo.ErpBpRoleRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectModelDetailRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaProvinsiRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaTenorRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaUserDetailRepository;
import id.co.adira.partner.param.repository.credsimparamrepo.ParaObjectModelRepository;
import id.co.adira.partner.param.util.ObjectConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static id.co.adira.partner.param.constant.AppEnum.*;
import static id.co.adira.partner.param.constant.LoggerEnum.*;
import static id.co.adira.partner.param.constant.ParamToBpEnum.*;

@Service
public class CommonParameterService {
	private final ParaProvinsiRepository paraProvinsiRepository;
	private final ParaUserDetailRepository paraUserDetailRepository;
	private final ParaObjectModelRepository paraObjectModelRepository;
	private final ParaObjectModelDetailRepository paraObjectModelDetailRepository;
	private final ErpBpRoleRepository erpBpRoleRepository;
	private final ParaTenorRepository paraTenorRepository;
	private final ObjectConverter objectConverter;

	@Autowired
	public CommonParameterService(ParaProvinsiRepository paraProvinsiRepository,
			ParaUserDetailRepository paraUserDetailRepository, ParaObjectModelRepository paraObjectModelRepository,
			ParaObjectModelDetailRepository paraObjectModelDetailRepository, ObjectConverter objectConverter,
			ParaTenorRepository paraTenorRepository, ErpBpRoleRepository erpBpRoleRepository) {
		this.paraProvinsiRepository = paraProvinsiRepository;
		this.paraUserDetailRepository = paraUserDetailRepository;
		this.paraObjectModelRepository = paraObjectModelRepository;
		this.paraObjectModelDetailRepository = paraObjectModelDetailRepository;
		this.objectConverter = objectConverter;
		this.paraTenorRepository = paraTenorRepository;
		this.erpBpRoleRepository = erpBpRoleRepository;
	}

	public static final Logger LOGGER = LoggerFactory.getLogger(CommonParameterService.class);

	@Cacheable(cacheNames = "cacheParamKelurahan", key = "#kelurahanName")
	public BaseResponse getKecamatanByKelurahanName(String kelurahanName) {
		BaseResponse baseResponse = new BaseResponse();

		KecamatanResponseDTO kecamatanResponseDTO = new KecamatanResponseDTO();
		try {
			getLocationDetailsKecamatanByKelurahanName(kecamatanResponseDTO, kelurahanName, baseResponse);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_KECAMATAN.getMessage());
		}

		return baseResponse;
	}

	@Cacheable(cacheNames = "cacheParamPortofolio", key = "#partnerCode")
	public BaseResponse getPortfolioByPartnerCode(String partnerCode) {
		BaseResponse baseResponse = new BaseResponse();

		try {

			mapUserDetailAndPortfolio(baseResponse, partnerCode);
			LOGGER.info(SUCCESS_GET_PORTFOLIO.getMessage(),
					objectConverter.convertObjectToString(baseResponse.getData()));

		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_USER_DETAIL.getMessage());
		}
		return baseResponse;
	}

	public BaseResponse getMaxTenor() {
		BaseResponse baseResponse = new BaseResponse();

		try {
			int maxTenor = paraTenorRepository.getMaxTenor();
			LOGGER.info(SUCCESS.getMessage() + " get max tenor");

			baseResponse.setStatus(HttpStatus.OK);
			baseResponse.setMessage(SUCCESS.getMessage());
			baseResponse.setData(maxTenor);

		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_DATA.getMessage());
		}
		return baseResponse;
	}

	private void mapUserDetailAndPortfolio(BaseResponse baseResponse, String partnerCode) {
		PortfolioHeadResponseDTO portfolioHeadResponseDTO = new PortfolioHeadResponseDTO();
		List<String> portfolios = new ArrayList<>();

		List<String> portfolioList = paraUserDetailRepository.getPortfolioByPartnerCode(partnerCode);
		for (String portfolio : portfolioList) {
			if (portfolio != null)
				portfolios.add(portfolio);
		}

		portfolioHeadResponseDTO.setPartnerCode(partnerCode);
		portfolioHeadResponseDTO.setPortfolios(portfolios);

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(portfolioHeadResponseDTO);
	}

	@Cacheable(cacheNames = "cacheParamKabKot", key = "#kabKotName")
	public BaseResponse getKabKotByName(String kabKotName) {
		BaseResponse baseResponse = new BaseResponse();

		try {
			getLocationDetailsKabKotByKabKotName(kabKotName, baseResponse);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_KABKOT.getMessage());
		}

		return baseResponse;
	}

	public BaseResponse getTipeUnit(TipeUnitRequestDTO tipeUnitRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			mapTipeUnit(baseResponse, tipeUnitRequestDTO);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_TIPE_UNIT.getMessage());
		}
		return baseResponse;
	}

	private void mapTipeUnit(BaseResponse baseResponse, TipeUnitRequestDTO tipeUnitRequestDTO)
			throws JsonProcessingException {
		TipeUnitHeadResponseDTO tipeUnitHeadResponseDTO = new TipeUnitHeadResponseDTO();

		List<TipeUnitDetailResponseDTO> tipeUnitDetailResponseDTOS = paraObjectModelRepository
				.findTipeUnitByBrandNameAndObjectCode(tipeUnitRequestDTO.getObjectBrandName(),
						tipeUnitRequestDTO.getObjectGroupId());

		tipeUnitHeadResponseDTO.setBrandName(tipeUnitRequestDTO.getObjectBrandName());
		tipeUnitHeadResponseDTO.setUnitList(tipeUnitDetailResponseDTOS);

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(tipeUnitHeadResponseDTO);

		LOGGER.info(SUCCESS_GET_TIPE_UNIT.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	public BaseResponse getBrandPartnerByPartnerCode(String partnerCode) {
		BaseResponse baseResponse = new BaseResponse();

		try {
			mapUserBrandBasedOnPartnerType(partnerCode, baseResponse);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_BRAND.getMessage());
		}
		return baseResponse;
	}

	public BaseResponse getLOVBrandTipeModel(LOVBtmRequestDTO lovBtmRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			mapLOVBrandTipeModel(baseResponse, lovBtmRequestDTO);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_LOV_BTM.getMessage());
		}
		return baseResponse;
	}

	private void mapLOVBrandTipeModel(BaseResponse baseResponse, LOVBtmRequestDTO LOVBtmRequestDTO)
			throws JsonProcessingException {
		LovBrandTipeModelHeadResponseDTO lovBrandTipeModelHeadResponseDTO = new LovBrandTipeModelHeadResponseDTO();
		List<LovBrandTipeModelDetailResponseDTO> lovBrandTipeModelDetailResponseDTOS = paraObjectModelDetailRepository
				.getLOVBrandTipeModelByObjectIdAndBrandName(LOVBtmRequestDTO.getObjectId(),
						LOVBtmRequestDTO.getBrandName());
		lovBrandTipeModelHeadResponseDTO.setBrandTipeDetails(lovBrandTipeModelDetailResponseDTOS);

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(lovBrandTipeModelHeadResponseDTO);

		LOGGER.info(SUCCESS_GET_LOV_BTM.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	private void insertErrorResponseWithException(BaseResponse baseResponse, Exception e, String errorMessage) {
		baseResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		baseResponse.setMessage(FAILED.getMessage());
		baseResponse.setData(THERE_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());

		LOGGER.error(errorMessage, e.getMessage());
	}

	private void insertErrorResponseNewWithException(BaseResponseNew baseResponse, Exception e, String errorMessage) {
		baseResponse.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		baseResponse.setStatus(1);
		baseResponse.setMessage(FAILED.getMessage());
		baseResponse.setData(THERE_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());

		LOGGER.error(errorMessage, e.getMessage());
	}

	private void mapUserBrandBasedOnPartnerType(String partnerCode, BaseResponse baseResponse)
			throws JsonProcessingException {
		BrandResponseDTO brandResponseDTO = new BrandResponseDTO();
		List<String> brandsFromDB = paraUserDetailRepository.getBrandByPartnerCode(partnerCode);
		List<String> brandList = new ArrayList<>();

		for (String brand : brandsFromDB) {
			if (brand == null || brand.equals(""))
				continue;
			else
				brandList.add(brand);
		}

		brandResponseDTO.setPartnerCode(partnerCode);
		brandResponseDTO.setBrandList(brandList);

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(brandResponseDTO);

		LOGGER.info(SUCCESS_GET_BRAND.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	private void getLocationDetailsKabKotByKabKotName(String kabKotName, BaseResponse baseResponse)
			throws JsonProcessingException {
		List<KabKotDetailResponseDTO> kabKot = paraProvinsiRepository
				.getLocationDetailsKabKotByKabKotName(kabKotName.toLowerCase());

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(kabKot);

		LOGGER.info(SUCCESS_GET_KABKOT.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	private void getLocationDetailsKecamatanByKelurahanName(KecamatanResponseDTO kecamatanResponseDTO,
			String kelurahanName, BaseResponse baseResponse) throws JsonProcessingException {
		List<KecamatanDetailResponseDTO> locationDetails = paraProvinsiRepository
				.getLocationDetailsByKelurahanName(kelurahanName.toLowerCase());
		kecamatanResponseDTO.setLocationDetails(locationDetails);

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(kecamatanResponseDTO);

		LOGGER.info(SUCCESS_GET_KECAMATAN.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	public BaseResponse getModelDetailDurable(String objectModelId) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			mapModelDetailDurable(baseResponse, objectModelId);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_MODEL_DETAIL.getMessage());
		}
		return baseResponse;
	}

	private void mapModelDetailDurable(BaseResponse baseResponse, String objectModelId) throws JsonProcessingException {
		List<ObjectModelDetailDurableResponseDTO> objectModelDetails = paraObjectModelDetailRepository
				.getModelDetailDurableOnly(objectModelId);

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());
		baseResponse.setData(objectModelDetails);

		LOGGER.info(SUCCESS_GET_MODEL_DETAIL.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	@Cacheable(cacheNames = "cacheParamModel", key = "{#modelRequestDTO.search, #modelRequestDTO.branchId, #modelRequestDTO.groupCode, #modelRequestDTO.dlc}")
	public BaseResponse getModel(ModelRequestDTO modelRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			mapGetModel(baseResponse, modelRequestDTO);
		} catch (Exception e) {
			insertErrorResponseWithException(baseResponse, e, ERROR_WHILE_GET_MODEL.getMessage());
		}
		return baseResponse;
	}

	private void mapGetModel(BaseResponse baseResponse, ModelRequestDTO modelRequestDTO)
			throws JsonProcessingException {
		ObjectModelDetailHeadResponseDTO objectModelDetailHeadResponseDTO = new ObjectModelDetailHeadResponseDTO();

		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		List<ErpBpDealerResponseDTO> erpBpDealerResponseDTO = paraObjectModelDetailRepository
				.getDealerCodeFromErp(modelRequestDTO.getDlc());
		if (modelRequestDTO.getDlc().length() > 6)
			insertDataWhenErpBpDealerDataIsNull(modelRequestDTO, objectModelDetailHeadResponseDTO, baseResponse);
		else
			insertDataWhenErpBpDealerDataExists(erpBpDealerResponseDTO.get(0), objectModelDetailHeadResponseDTO,
					baseResponse, modelRequestDTO);

		LOGGER.info(SUCCESS_GET_MODEL_DETAIL.getMessage(), objectConverter.convertObjectToString(baseResponse));
	}

	public void insertDataWhenErpBpDealerDataExists(ErpBpDealerResponseDTO erpBpDealerResponseDTO,
			ObjectModelDetailHeadResponseDTO objectModelDetailHeadResponseDTO, BaseResponse baseResponse,
			ModelRequestDTO modelRequestDTO) {
		List<ParaUserDetail> listUserDetail = new ArrayList<>();
		List<ParaUserDetail> listUserDetailNew = new ArrayList<>();
		List<ObjectModelDetailDetailResponseDTO> objectModelDetails = new ArrayList<>();
		var brandDlc = "";

		listUserDetail = paraUserDetailRepository.getUserDetailByDlc(modelRequestDTO.getDlc());
		if (modelRequestDTO.getGroupCode().equalsIgnoreCase("001")) {
			listUserDetailNew = listUserDetail.stream().filter(e -> e.getPortfolio().equalsIgnoreCase("NMCY")
					&& (e.getBrand()!= null) && !e.getBrand().equalsIgnoreCase("ALL") && e.getIsActive() == 0)
					.collect(Collectors.toList());

			if (listUserDetailNew.size() > 0 && !listUserDetailNew.get(0).getBrand().equalsIgnoreCase("ALL")
					&& listUserDetailNew.get(0).getPortfolio().equalsIgnoreCase("NMCY")) {
				brandDlc = listUserDetailNew.get(0).getBrand();
			}

		} else if (modelRequestDTO.getGroupCode().equalsIgnoreCase("003")) {

			listUserDetailNew = listUserDetail.stream()
					.filter(e -> e.getPortfolio().equalsIgnoreCase("NCAR") && !e.getBrand().equalsIgnoreCase("ALL"))
					.collect(Collectors.toList());
			if (listUserDetailNew.size() > 0 && !listUserDetailNew.get(0).getBrand().equalsIgnoreCase("ALL")
					&& listUserDetailNew.get(0).getPortfolio().equalsIgnoreCase("NCAR")) {
				brandDlc = listUserDetailNew.get(0).getBrand();
			}
		} else {

			brandDlc = "";
		}

		if (brandDlc.isBlank() || brandDlc == "") {
			objectModelDetails = paraObjectModelDetailRepository.getModelDetailByObjectIdAndSearch(
					modelRequestDTO.getGroupCode(), modelRequestDTO.getSearch().toUpperCase(),
					modelRequestDTO.getDlc());
		} else {
			objectModelDetails = paraObjectModelDetailRepository.getModelDetailByObjectIdAndBrand(
					modelRequestDTO.getGroupCode(), modelRequestDTO.getSearch().toUpperCase(), modelRequestDTO.getDlc(),
					brandDlc);
		}

		objectModelDetailHeadResponseDTO.setModelDetails(objectModelDetails);
		baseResponse.setData(objectModelDetailHeadResponseDTO);

	}

	private void insertDataWhenErpBpDealerDataIsNull(ModelRequestDTO modelRequestDTO,
			ObjectModelDetailHeadResponseDTO objectModelDetailHeadResponseDTO, BaseResponse baseResponse) {
		List<ObjectModelDetailDetailResponseDTO> objectModelDetailDetailResponseDTOS = paraObjectModelRepository
				.findObjectModelIfErpIsNullAndDlcLengthIsNotEqualsThanSix(modelRequestDTO.getSearch().toUpperCase(),
						modelRequestDTO.getGroupCode());

		objectModelDetailHeadResponseDTO.setModelDetails(objectModelDetailDetailResponseDTOS);
		baseResponse.setData(objectModelDetailHeadResponseDTO);
	}

	public String checkBpRoleCategory(String partnerType) {
		var roleCatBp = "000";
		if (partnerType.contentEquals(DEALER.getPartnerType())) {
			roleCatBp = DEALER.getRoleCatBp();
		}
		if (partnerType.contentEquals(MERCHANT.getPartnerType())) {
			roleCatBp = MERCHANT.getRoleCatBp();
		}
		if (partnerType.contentEquals(AXI.getPartnerType())) {
			roleCatBp = AXI.getRoleCatBp();
		}
		if (partnerType.contentEquals(KEDAY.getPartnerType())) {
			roleCatBp = KEDAY.getRoleCatBp();
		}

		return roleCatBp;
	}

	public BaseResponseNew getListPartner(PartnerIdRequestDTO partnerIdRequestDTO) {
		BaseResponseNew baseResponse = new BaseResponseNew();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(SUCCESS.getMessage());

		try {
			var search = "%".concat(partnerIdRequestDTO.getSearch().toUpperCase().concat("%"));
			var roleCatBp = checkBpRoleCategory(partnerIdRequestDTO.getPartnerType());
			List<PartnerIdResponseDTO> partnerList = erpBpRoleRepository.getPartnerIdBySearch(roleCatBp, search);
			if (!partnerList.isEmpty()) {
				baseResponse.setData(partnerList);
			} else {
				baseResponse.setStatus(1);
				baseResponse.setMessage(DATA_NOT_FOUND.getMessage());
			}

		} catch (Exception e) {
			insertErrorResponseNewWithException(baseResponse, e, ERROR_WHILE_GET_DATA.getMessage());
		}
		return baseResponse;
	}

}
