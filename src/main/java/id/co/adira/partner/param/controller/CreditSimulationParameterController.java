package id.co.adira.partner.param.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.adira.partner.param.dto.request.*;
import id.co.adira.partner.param.dto.response.BaseResponse;
import id.co.adira.partner.param.service.CreditSimulationParameterService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/credsim/param")
public class CreditSimulationParameterController {

    private final CreditSimulationParameterService creditSimulationParameterService;

    @Autowired
    public CreditSimulationParameterController(CreditSimulationParameterService creditSimulationParameterService) {
        this.creditSimulationParameterService = creditSimulationParameterService;
    }

    @PostMapping("/get-biaya-admin")
    @ApiOperation(value = "Get Biaya Admin", nickname = "Get Biaya Admin")
    //@CrossOrigin
    public ResponseEntity<Object> getBiayaAdmin(@RequestBody BiayaAdminRequestDTO biayaAdminRequestDTO) throws JsonProcessingException {
        BaseResponse baseResponse = creditSimulationParameterService.getBiayaAdmin(biayaAdminRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    @PostMapping("/get-effective-or-landing-rate")
    @ApiOperation(value = "Get Effective or Lending Rate", nickname = "Get Effective or Lending Rate")
    //@CrossOrigin
    public ResponseEntity<Object> getEffectiveOrLandingRate(@RequestBody EffectiveOrLandingRateRequestDTO effectiveOrLandingRateRequestDTO){
        BaseResponse baseResponse = creditSimulationParameterService.getEffectiveOrLandingRate(effectiveOrLandingRateRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    @GetMapping("/get-insurance-rate")
    @ApiOperation(value = "Get Insurance Rate", nickname = "Get Insurance Rate by Program Id")
    //@CrossOrigin
    public ResponseEntity<Object> getInsuranceRateByProgramId(@RequestParam String programId){
        BaseResponse baseResponse = creditSimulationParameterService.getInsuranceRateByProgramId(programId);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    //API ini untuk sementara di hold dulu.
    //Nanti kalau ada perubahan atau penambahan parameter tinggal pake yg ini aja.
    //Untuk sementara, parameter nya pakai programId dulu.
    @GetMapping("/get-data-program")
    @ApiOperation(value = "Get Data Program", nickname = "Get Data Program")
    //@CrossOrigin
    public ResponseEntity<Object> getDataProgram(@RequestParam String programId){
        BaseResponse baseResponse = creditSimulationParameterService.getDataProgramByProgramId(programId);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    //Currently used
    @GetMapping("/get-all-data-program")
    @ApiOperation(value = "Get All Data Program", nickname = "Get All Data Program")
    //@CrossOrigin
    public ResponseEntity<Object> getAllDataProgram(){
        BaseResponse baseResponse = creditSimulationParameterService.getAllDataProgram();
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    //Currently used
    @GetMapping("/get-installment-type")
    @ApiOperation(value = "Get Installment Type", nickname = "Get Installment Type")
    //@CrossOrigin
    public ResponseEntity<Object> getInstallmentType(){
        BaseResponse baseResponse = creditSimulationParameterService.getInstallmentType();
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    @PostMapping("/get-min-max-pencairan")
    @ApiOperation(value = "Get Min Max Value Pencairan", nickname = "Get Min Max Value Pencairan")
    //@CrossOrigin
    public ResponseEntity<Object> getMinMaxPencairan(@RequestBody @Valid MinMaxPencairanRequestDTO minMaxPencairanRequestDTO){
        BaseResponse baseResponse = creditSimulationParameterService.getMinMaxPencairan(minMaxPencairanRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    @PostMapping("/calculate-cicilan")
    @ApiOperation(value = "Hitung Cicilan", nickname = "Hitung Cicilan")
    //@CrossOrigin
    public ResponseEntity<Object> calculateCicilan(@RequestBody @Valid CalculateCicilanRequestDTO calculateCicilanRequestDTO) throws JsonProcessingException {
        BaseResponse baseResponse = creditSimulationParameterService.calculateCicilan(calculateCicilanRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

	/*
	 * @PostMapping("/get-asuransi-cash-and-credit")
	 * 
	 * @ApiOperation(value = "Get Asuransi Cash or Credit", nickname =
	 * "Get Asuransi Cash or Credit") //@CrossOrigin public ResponseEntity<Object>
	 * getAsuransiCashOrCredit(@RequestBody AsuransiCashAndCreditRequestDTO
	 * asuransiCashAndCreditRequestDTO){ BaseResponse baseResponse =
	 * creditSimulationParameterService.getAsuransiCashAndCredit(
	 * asuransiCashAndCreditRequestDTO); return new ResponseEntity<>(baseResponse,
	 * baseResponse.getStatus()); }
	 */

    @PostMapping("/get-dp-value-for-datun")
    @ApiOperation(value = "Get DP Value for Dana Tunai", nickname = "Get Dp Value for Dana Tunai")
    //@CrossOrigin
    public ResponseEntity<Object> getOtr(@RequestBody DpValueForDanaTunaiRequestDTO dpValueForDanaTunaiRequestDTO){
        BaseResponse baseResponse = creditSimulationParameterService.getDpValueForDatun(dpValueForDanaTunaiRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getStatus());
    }

    @PostMapping("/get-otr")
    @ApiOperation(value = "Get Otr from MarketPrice", nickname = "Get Otr from MarketPrice")
    //@CrossOrigin
    public ResponseEntity<Object> getOtrFromLtv(@RequestBody OtrRequestDTO otrRequestDTO){
        BaseResponse  response = creditSimulationParameterService.getOtrFromLtv(otrRequestDTO);
        return new ResponseEntity<>(response, response.getStatus());
    }

}
