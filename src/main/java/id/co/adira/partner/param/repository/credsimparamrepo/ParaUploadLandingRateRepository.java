package id.co.adira.partner.param.repository.credsimparamrepo;

import id.co.adira.partner.param.dto.response.EffectiveOrLandingRateResTempDTO;
import id.co.adira.partner.param.dto.response.EffectiveOrLandingRateResponseDTO;
import id.co.adira.partner.param.entity.ParaUploadLandingRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
@Transactional
public interface ParaUploadLandingRateRepository extends JpaRepository<ParaUploadLandingRate, String> {

    @Query("select distinct new id.co.adira.partner.param.dto.response.EffectiveOrLandingRateResponseDTO(pulr.paraLandingRate)" +
            "from ParaUploadLandingRate pulr " +
            "inner join ParaUserDetail pud on " +
            "pud.userId = :pic " +
            "and (pulr.paraDealOutletId = pud.partnerCode or pulr.paraDealOutletId = 'ALL')  " +
            "where (pulr.paraObjectModelId = :objectModelId or pulr.paraObjectModelId = 'ALL') " +
            "and (pulr.paraObjectId = :objectId or pulr.paraObjectId = 'ALL') " +
            "and (pulr.paraObjectTypeId = :objectTypeId or pulr.paraObjectTypeId = 'ALL') " +
            "and (pulr.paraObjectBrandId = :objectBrandId or pulr.paraObjectBrandId = 'ALL') " +
            "and pulr.paraProductProgramId = :programId " +
            "and pulr.paraMaxTenor >= :tenor " +
            "and pulr.paraMinTenor <= :tenor " +
            "and pulr.active = 0")
    EffectiveOrLandingRateResponseDTO findEffectiveOrLandingRate(String objectModelId,
                                                                 BigDecimal tenor, String pic,
                                                                 String objectId, String objectTypeId,
                                                                 String objectBrandId, String programId);

    @Query("select distinct new id.co.adira.partner.param.dto.response.EffectiveOrLandingRateResTempDTO(pulr.paraLandingRate, pulr.createdDate) " +
            "from ParaUploadLandingRate pulr join ParaUserDetail pud on " +
            "pud.userId = :pic and (pulr.paraDealOutletId = 'ALL' or pulr.paraDealOutletId = pud.partnerName) " +
            "where pulr.paraObjectModelId = 'ALL' " +
            "and pulr.paraObjectId = :objectId " +
            "and pulr.paraObjectTypeId = 'ALL' " +
            "and pulr.paraObjectBrandId = 'ALL' " +
            "and pulr.paraMaxTenor >= :tenor " +
            "and pulr.paraMinTenor <= :tenor " +
            "order by pulr.createdDate desc")
    List<EffectiveOrLandingRateResTempDTO> findEffectiveOrLandingRateWithAllParam(String pic, String objectId, BigDecimal tenor);
}
