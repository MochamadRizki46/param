package id.co.adira.partner.param.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "erp_bp_header")
@Data
public class ErpBpHeader {
    @Id
    @Column(name = "erp_bp_grouping")
    String erpBpGrouping;

    @Column(name = "erp_bp_bpartner")
    String erpBpBpartner;

    @Column(name = "erp_bp_searchterm1")
    String erpBpSearchterm1;

    @Column(name = "erp_bp_searchterm2")
    String erpBpSearchterm2;

    @Column(name = "erp_bp_legalform")
    String erpBplegalform;

    @Column(name = "erp_bp_title_key")
    String erpBpTit;

    @Column(name = "erp_bp_name1")
    String erpBpName1;

    @Column(name = "erp_bp_name2")
    String erpBpName2;

    @Column(name = "erp_bp_birthplace")
    String erpBpBirthplace;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "erp_bp_birthdate")
    private Date erpBpBirthdate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "erp_bp_foundation")
    private Date erpBpFoundation;

    @Column(name = "erp_bp_sex")
    String erpBpSex;

    @Column(name = "erp_bp_maritalstatus")
    String erpBpMaritalstatus;

    @Column(name = "erp_bp_occupation")
    String erpBpOccupation;

    @Column(name = "erp_bp_zstat")
    String erpBpZstat;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "erp_bp_zdate")
    private Date erpBpZdate;

    @Column(name = "erp_bp_zhour")
    String erpBpZhour;

    @Column(name = "erp_bp_zuser")
    String erpBpZuser;

    @Column(name = "erp_bp_request_number")
    String erpBpRequestNumber;

    @Column(name = "erp_bp_type")
    String erpBpType;

    @Column(name = "erp_bp_category")
    String erpBpCategory;

    @Column(name = "erp_bp_zcreator")
    String erpBpZcreator;

    @Column(name = "erp_bp_timestamp")
    String erpBpTimestamp;


}
