CREATE TABLE if not exists public.para_upld_deal_qlty (
	para_object_id varchar(5) NOT NULL,
	para_deal_outlet_id varchar(11) NOT NULL,
	para_qlty_val numeric NOT NULL,
	para_delta_period timestamp NULL,
	para_flag_source numeric NOT NULL,
	start_date timestamp NULL,
	end_date timestamp NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	para_deal_qlty_id varchar(5) NOT NULL,
	CONSTRAINT para_upld_deal_qlty_uk UNIQUE (para_object_id, para_deal_outlet_id, para_qlty_val, para_delta_period, para_flag_source, start_date, end_date, para_deal_qlty_id)
);