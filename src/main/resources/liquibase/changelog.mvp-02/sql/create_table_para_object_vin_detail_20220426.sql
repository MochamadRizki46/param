CREATE TABLE if not exists public.para_object_vin_detail (
	para_object_vin_detail_id varchar(10) NOT NULL,
	para_object_vin_id varchar(10) NOT NULL,
	para_object_year varchar(4) NULL,
	para_price numeric(30,5) NULL,
	para_region varchar(20) NULL,
	effective_date timestamp NULL,
	active int4 NULL,
	logid numeric NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NULL,
	para_dealer_code text NULL,
	CONSTRAINT para_object_vin_detail_pkey PRIMARY KEY (para_object_vin_detail_id),
	CONSTRAINT "fk_ObjectVinDtl_ObjectVin" FOREIGN KEY (para_object_vin_id) REFERENCES para_object_vin(para_object_vin_id)
);