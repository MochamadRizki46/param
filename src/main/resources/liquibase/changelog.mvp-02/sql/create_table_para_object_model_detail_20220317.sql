CREATE TABLE IF NOT EXISTS public.para_object_model_detail (
	para_object_model_detail_id varchar(10) NOT NULL,
	para_object_model_id varchar(10) NOT NULL,
	para_object_model_detail varchar(100) NOT NULL,
	para_object_model_detail_desc varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NOT NULL,
	para_foto_unit bytea NULL,
	para_document bytea NULL,
	CONSTRAINT para_object_model_detail_pkey PRIMARY KEY (para_object_model_detail_id)
);


