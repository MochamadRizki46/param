CREATE TABLE if not exists public.para_cp_unit_cust (
	para_cp_unit_id varchar(4) NOT NULL,
	para_kelurahan_id varchar(5) NOT NULL,
	start_date timestamp NOT NULL,
	end_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_cp_unit_cust_pkey PRIMARY KEY (para_kelurahan_id)
);


-- public.para_cp_unit_cust foreign keys

ALTER TABLE public.para_cp_unit_cust ADD CONSTRAINT "fk_CpUnitCust_CpUnit" FOREIGN KEY (para_cp_unit_id) REFERENCES public.para_cp_unit(para_cp_unit_id);
ALTER TABLE public.para_cp_unit_cust ADD CONSTRAINT "fk_CpUnitCust_Kelurahan" FOREIGN KEY (para_kelurahan_id) REFERENCES public.para_kelurahan(para_kelurahan_id);