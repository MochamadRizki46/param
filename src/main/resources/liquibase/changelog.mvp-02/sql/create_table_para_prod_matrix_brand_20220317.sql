CREATE TABLE IF NOT EXISTS public.para_prod_matrix_brand (
	para_prod_matrix_id varchar(5) NOT NULL,
	para_prod_sales_group_id varchar(5) NOT NULL,
	para_object_brand_id varchar(5) NOT NULL,
	para_prod_matrix_brand_id varchar(5) NOT NULL,
	para_prod_matrix_brand_desc varchar(50) NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_prod_matrix_brand_pkey PRIMARY KEY (para_prod_matrix_brand_id)
);


