CREATE TABLE IF NOT EXISTS public.para_appl_status_group (
	para_appl_status_group_id varchar(10) NOT NULL,
	para_appl_status_group_desc varchar(255) NULL,
	effective_date timestamp NULL,
	active bool NULL,
	created_date timestamp NULL,
	created_by varchar(50) NULL,
	modified_date timestamp NULL,
	modified_by varchar(50) NULL,
	CONSTRAINT para_appl_status_group_pk PRIMARY KEY (para_appl_status_group_id)
);