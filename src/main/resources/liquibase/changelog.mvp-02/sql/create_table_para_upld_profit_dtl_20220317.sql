CREATE TABLE IF NOT EXISTS public.para_upld_profit_dtl (
	para_upld_profit_dtl_id uuid NOT NULL,
	para_upld_profit_id uuid NOT NULL,
	para_tenor numeric NOT NULL,
	para_landing_rate numeric NULL,
	para_inst_payment numeric NULL,
	para_payment_type_id varchar(5) NOT NULL,
	para_tac_2 numeric NOT NULL,
	para_tac_3 numeric NOT NULL,
	para_insr_coc numeric NULL,
	profit_flag bool NULL,
	start_date timestamp NOT NULL,
	end_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_upld_profit_dtl_pkey PRIMARY KEY (para_upld_profit_dtl_id)
);


