CREATE TABLE IF NOT EXISTS public.para_upld_profit_fee (
	para_upld_profit_fee_id uuid NOT NULL,
	para_upld_profit_dtl_id uuid NOT NULL,
	para_fee_type_code varchar(3) NOT NULL,
	para_fee_cash numeric NOT NULL,
	para_fee_credit numeric NOT NULL,
	start_date timestamp NOT NULL,
	end_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_upld_profit_fee_pkey PRIMARY KEY (para_upld_profit_fee_id)
);


