CREATE TABLE if not exists public.para_object_made_in (
	para_object_made_in_id varchar(5) NOT NULL,
	para_object_made_in_name varchar(50) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NOT NULL,
	CONSTRAINT para_object_made_in_pkey PRIMARY KEY (para_object_made_in_id)
);