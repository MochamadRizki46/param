CREATE TABLE IF NOT EXISTS public.para_bran_info (
	bran_company_id varchar(50) NOT NULL,
	bran_br_id varchar(4) NOT NULL,
	bran_parent_id varchar(4) NULL,
	bran_br_name varchar(30) NULL,
	bran_telph1 varchar(12) NULL,
	bran_telph2 varchar(12) NULL,
	bran_address1 varchar(25) NULL,
	bran_address2 varchar(25) NULL,
	bran_zip varchar(5) NULL,
	bran_city varchar(20) NULL,
	bran_bm_name varchar(30) NULL,
	bran_adh_name varchar(30) NULL,
	bran_mh_name varchar(30) NULL,
	bran_pic_bpkb varchar(30) NULL,
	bran_bpkb_period numeric NULL,
	bran_bpkb_turnback_period numeric NULL,
	bran_pickup_period numeric NULL,
	bran_writeoff_period numeric NULL,
	bran_stop_accrue_period numeric NULL,
	bran_pnlt_type varchar(1) NULL,
	bran_daily_pnlt_amnt_exp numeric NULL,
	bran_pnlt_rate_exp numeric NULL,
	bran_memo varchar(50) NULL,
	bran_adm_fee numeric NULL,
	bran_insr_fee numeric NULL,
	bran_stamp_fee numeric NULL,
	bran_net_dp_percent numeric NULL,
	bran_rounded_value numeric NULL,
	bran_log_id varchar(38) NULL,
	bran_deleted int4 NULL,
	bran_af_name varchar(30) NULL,
	brand_lo_name varchar(30) NULL,
	bran_cp_name varchar(30) NULL,
	bran_aloc_code varchar(12) NULL,
	bran_min_age numeric NULL,
	bran_max_age numeric NULL,
	bran_migr_date timestamp NULL,
	bran_prod_year_min numeric NULL,
	bran_app_ver varchar(6) NULL,
	bran_last_day_closing timestamp NULL,
	bran_pc_vch_max_limit numeric NULL,
	bran_pt_date_change varchar(1) NULL,
	bran_pnlt_bdebt_1 numeric NULL,
	bran_pnlt_bdebt_rate_1 numeric NULL,
	bran_pnlt_bdebt_2 numeric NULL,
	bran_pnlt_bdebt_rate_2 numeric NULL,
	bran_pnlt_bdebt_3 numeric NULL,
	bran_pnlt_bdebt_rate_3 numeric NULL,
	bran_pnlt_bdebt_4 numeric NULL,
	bran_pnlt_bdebt_rate_4 numeric NULL,
	bran_pnlt_bdebt_5 numeric NULL,
	bran_pnlt_bdebt_rate_5 numeric NULL,
	bran_mtr_min_admin_fee numeric NULL,
	bran_mbl_min_admin_fee numeric NULL,
	bran_insf_calc_mode varchar(1) NULL,
	bran_fin_name varchar(30) NULL,
	bran_pnlt_target_1 numeric NULL,
	bran_pnlt_target_rate_1 numeric NULL,
	bran_pnlt_target_2 numeric NULL,
	bran_pnlt_target_rate_2 numeric NULL,
	bran_pnlt_target_3 numeric NULL,
	bran_pnlt_target_rate_3 numeric NULL,
	bran_pnlt_target_4 numeric NULL,
	bran_pnlt_target_rate_4 numeric NULL,
	bran_aging_od numeric NULL,
	bran_piut_kary_code varchar(12) NULL,
	bran_os_drkh numeric NULL,
	bran_bpkb_loc numeric NULL,
	bran_kpi_walr numeric NULL,
	bran_kpi_min_dp numeric NULL,
	bran_kpi_avg_insr numeric NULL,
	bran_min_closing numeric NULL,
	bran_max_upping_intr numeric NULL,
	bran_max_pdh numeric NULL,
	bran_beg_deskcoll numeric NULL,
	bran_end_deskcoll numeric NULL,
	bran_beg_hr numeric NULL,
	bran_end_hr numeric NULL,
	bran_provisi varchar(4) NULL,
	bran_queue_status numeric NULL,
	bran_custodian_name varchar(50) NULL,
	bran_custodian_address1 varchar(50) NULL,
	bran_custodian_address2 varchar(50) NULL,
	bran_insr_amt_limit numeric NULL,
	bran_month_corr_top numeric NULL,
	bran_ibukota_provinsi varchar(20) NULL,
	bran_pnlt_plns numeric NULL,
	bran_refund_adm_flag varchar(1) NULL,
	bran_bpkb_periode_amu numeric NULL,
	bran_bins_rounded_value numeric NULL,
	bran_max_fdp numeric NULL,
	bran_min_net_dp numeric NULL,
	bran_max_tenor_limit numeric NULL,
	bran_min_adm_limit numeric NULL,
	bran_mayor numeric NULL,
	bran_ora_cutoff_date timestamp NULL,
	bran_fdp varchar(14) NULL,
	bran_ms2_addr varchar(50) NULL,
	bran_sys_lastday_closing timestamp NULL,
	bran_dsp_id varchar(3) NULL,
	bran_stop_closing timestamp NULL,
	bran_max_grace_period numeric NULL,
	bran_fleet varchar(21) NULL,
	bran_orafin_closing timestamp NULL,
	bran_disbursement_flag varchar(1) NULL,
	bran_rate_renewal_insr numeric NULL,
	bran_closing_amore_fase1 timestamp NULL,
	bran_lastday_amor timestamp NULL,
	bran_area_opr varchar(4) NULL,
	bran_monthly_closing timestamp NULL,
	bran_lastday_sglnt_flag timestamp NULL,
	bran_eom_ar_scoring timestamp NULL,
	bran_max_count_ppd numeric NULL,
	bran_pool_id timestamp NULL,
	bran_ms2_mcs numeric NULL,
	bran_eod_ar_scoring timestamp NULL,
	bran_ovd_bpkb numeric NULL,
	bran_category varchar(2) NULL,
	bran_taxid_adira varchar(11) NULL,
	bran_efektif_taxid_date timestamp NULL,
	bran_aloc_code_new varchar(6) NULL,
	bran_sentraid varchar(8) NULL,
	bran_region_id varchar(8) NULL,
	bran_region_name varchar(30) NULL,
	bran_region_acct_no varchar(20) NULL,
	bran_region_bi_code varchar(7) NULL,
	bran_region_bank_city varchar(25) NULL,
	bran_region_bank_branch varchar(50) NULL,
	bran_flag_fclos varchar(2) NULL,
	bran_fclos_date timestamp NULL,
	bran_eff_cops timestamp NULL,
	bran_acct_start_date timestamp NULL,
	bran_acct_end_date timestamp NULL,
	CONSTRAINT para_bran_info_pkey PRIMARY KEY (bran_company_id, bran_br_id)
);