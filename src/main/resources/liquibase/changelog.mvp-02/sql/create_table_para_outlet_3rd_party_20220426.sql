CREATE TABLE if not exists public.para_outlet_3rd_party (
	para_outlet_3rd_party_id varchar(3) NOT NULL,
	para_outlet_3rd_party_name varchar(100) NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NOT NULL,
	CONSTRAINT para_outlet_3rd_party_pkey PRIMARY KEY (para_outlet_3rd_party_id)
);