CREATE TABLE IF NOT EXISTS public.para_provinsi (
	para_provinsi_id varchar(2) NOT NULL,
	para_provinsi_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_provinsi_pkey PRIMARY KEY (para_provinsi_id)
);