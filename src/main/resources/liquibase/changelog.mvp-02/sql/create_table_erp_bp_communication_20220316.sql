CREATE TABLE IF NOT EXISTS public.erp_bp_communication (
	erp_bp_bpartner varchar(10) NULL,
	erp_bp_telephone varchar(30) NULL,
	erp_bp_extension varchar(10) NULL,
	erp_bp_fax_no varchar(30) NULL,
	erp_bp_e_mail varchar(241) NULL,
	erp_bp_zpic_vnd varchar(40) NULL,
	erp_bp_zpic_cbg varchar(40) NULL,
	erp_bp_zcbg varchar(40) NULL,
	erp_bp_request_number varchar(10) NULL,
	erp_bp_zjabatan varchar(40) NULL,
	erp_bp_item_number numeric NULL,
	erp_bp_timestamp timestamp NULL,
	CONSTRAINT erp_bp_communication_uk UNIQUE (erp_bp_bpartner, erp_bp_request_number, erp_bp_item_number)
);