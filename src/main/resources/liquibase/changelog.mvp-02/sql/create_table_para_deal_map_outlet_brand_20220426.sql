CREATE TABLE if not exists public.para_deal_map_outlet_brand (
	para_deal_outlet_id varchar(11) NOT NULL,
	para_object_brand_id varchar(5) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_deal_map_outlet_brand_uk UNIQUE (para_deal_outlet_id, para_object_brand_id)
);