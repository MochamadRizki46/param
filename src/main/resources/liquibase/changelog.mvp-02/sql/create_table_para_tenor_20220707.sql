-- public.para_tenor definition

-- Drop table

-- DROP TABLE public.para_tenor;

CREATE TABLE public.para_tenor (
	para_tenor_id varchar(3) NOT NULL,
	para_tenor_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_tenor_para_tenor_id_key UNIQUE (para_tenor_id)
);