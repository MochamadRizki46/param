CREATE TABLE IF NOT EXISTS public.para_fee_type (
	para_fee_type_id varchar(2) NOT NULL,
	para_fee_type_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_fee_type_pkey PRIMARY KEY (para_fee_type_id)
);