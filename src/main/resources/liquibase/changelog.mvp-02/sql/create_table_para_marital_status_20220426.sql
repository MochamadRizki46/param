CREATE TABLE if not exists public.para_marital_status (
	para_marital_status_id varchar(3) NOT NULL,
	para_marital_status_name varchar(100) NULL,
	para_marital_status_score numeric(18,5) NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_marital_status_pkey PRIMARY KEY (para_marital_status_id)
);