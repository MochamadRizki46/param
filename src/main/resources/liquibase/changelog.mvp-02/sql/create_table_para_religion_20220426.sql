CREATE TABLE if not exists public.para_religion (
	para_religion_id varchar(3) NOT NULL,
	para_religion_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_religion_pkey PRIMARY KEY (para_religion_id)
);