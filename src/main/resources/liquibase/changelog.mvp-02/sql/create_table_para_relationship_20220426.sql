CREATE TABLE if not exists public.para_relationship (
	para_relationship_id varchar(3) NOT NULL,
	para_relationship_name varchar(100) NULL,
	effective_date timestamp NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_relationship_pkey PRIMARY KEY (para_relationship_id)
);