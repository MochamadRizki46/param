CREATE TABLE IF NOT EXISTS public.para_kelurahan (
	para_kecamatan_id varchar(5) NOT NULL,
	para_kelurahan_id varchar(5) NOT NULL,
	para_kelurahan_name varchar(100) NOT NULL,
	para_zipcode varchar(5) NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_kelurahan_pkey PRIMARY KEY (para_kelurahan_id)
);


