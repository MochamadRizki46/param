CREATE TABLE IF NOT EXISTS public.para_object (
	para_object_id varchar(4) NOT NULL,
	para_object_group_id varchar(5) NOT NULL,
	para_object_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NOT NULL,
	CONSTRAINT para_object_pkey PRIMARY KEY (para_object_id)
);


