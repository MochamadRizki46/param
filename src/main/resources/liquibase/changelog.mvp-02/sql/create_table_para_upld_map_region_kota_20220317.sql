CREATE TABLE IF NOT EXISTS public.para_upld_map_region_kota (
	para_upld_map_region_kota_id varchar(5) NOT NULL,
	para_cp_area_id varchar(5) NOT NULL,
	para_kabkot_id varchar(5) NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_upld_map_region_kota_pkey PRIMARY KEY (para_upld_map_region_kota_id)
);