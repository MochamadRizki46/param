CREATE TABLE IF NOT EXISTS public.erp_bp_relationship (
	erp_bp_bpartner varchar(10) NULL,
	erp_bp_partner2 varchar(10) NULL,
	erp_bp_relat_category varchar(6) NULL,
	erp_bp_zvalid_from_prt timestamp NULL,
	erp_bp_zvalid_to_prt timestamp NULL,
	erp_bp_request_number varchar(10) NULL,
	erp_bp_timestamp timestamp NULL,
	erp_bp_item_number numeric NULL,
	CONSTRAINT erp_bp_relationship_uk UNIQUE (erp_bp_bpartner, erp_bp_request_number, erp_bp_item_number)
);