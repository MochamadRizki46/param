CREATE TABLE if not exists public.erp_bp_industry (
	erp_bp_bpartner varchar(10) NULL,
	erp_bp_keysystem varchar(4) NULL,
	erp_bp_ind_sector varchar(10) NULL,
	erp_bp_ind_default varchar(1) NULL,
	erp_bp_request_number varchar(10) NULL,
	erp_bp_timestamp timestamp NULL,
	erp_bp_item_number numeric NULL,
	CONSTRAINT erp_bp_industry_uk UNIQUE (erp_bp_bpartner, erp_bp_request_number, erp_bp_item_number)
);