CREATE TABLE IF NOT EXISTS public.mst_system (
	system_code varchar(60) NOT NULL,
	system_type_code varchar(100) NOT NULL,
	system_value varchar(2000) NOT NULL,
	system_desc varchar(4000) NOT NULL,
	system_seq int4 NOT NULL,
	disabled_flag bool NOT NULL,
	create_date timestamp NOT NULL,
	create_by varchar(60) NOT NULL,
	update_date timestamp NULL,
	update_by varchar(60) NULL,
	CONSTRAINT mst_system_pkey PRIMARY KEY (system_code)
);