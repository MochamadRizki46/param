CREATE TABLE if not exists public.para_appl_status (
	para_appl_status_sk_id uuid NOT NULL, -- STATUS SK ID (UUID) AUTO GENERATED
	para_appl_status_id varchar(10) NULL, -- APPLICATION STATUS ID
	para_appl_status_group_id varchar(10) NULL, -- APPLICATION STATUS GROUP ID
	para_appl_status_desc varchar(255) NULL, -- APPLICATION STATUS DESCRIPTION
	squad varchar(50) NULL, -- SQUAD INFO
	level1 varchar(255) NULL, -- LEVEL1
	level2 varchar(255) NULL, -- LEVEL2
	level3 varchar(255) NULL, -- LEVEL3
	effective_date timestamp NULL, -- EFFECTIVE DATE
	active int4 NULL, -- ACTIVE (0: ACTIVE, 1: NONACTIVE)
	created_date timestamp NULL, -- CREATED DATE
	created_by varchar(50) NULL, -- CREATED BY
	modified_date timestamp NULL, -- MODIFIED DATE
	modified_by varchar(50) NULL, -- MODIFIED BY
	CONSTRAINT para_appl_status_pk PRIMARY KEY (para_appl_status_sk_id),
	CONSTRAINT para_appl_status1_fk FOREIGN KEY (para_appl_status_group_id) REFERENCES para_appl_status_group(para_appl_status_group_id),
	CONSTRAINT para_appl_status_fk FOREIGN KEY (para_appl_status_group_id) REFERENCES para_appl_status_group(para_appl_status_group_id)
);
CREATE INDEX para_appl_status_level2_idx ON public.para_appl_status USING btree (level2);
COMMENT ON TABLE public.para_appl_status IS 'A table to contain all PARA_APPL_STATUS';

-- Column comments

COMMENT ON COLUMN public.para_appl_status.para_appl_status_sk_id IS 'STATUS SK ID (UUID) AUTO GENERATED';
COMMENT ON COLUMN public.para_appl_status.para_appl_status_id IS 'APPLICATION STATUS ID';
COMMENT ON COLUMN public.para_appl_status.para_appl_status_group_id IS 'APPLICATION STATUS GROUP ID';
COMMENT ON COLUMN public.para_appl_status.para_appl_status_desc IS 'APPLICATION STATUS DESCRIPTION';
COMMENT ON COLUMN public.para_appl_status.squad IS 'SQUAD INFO';
COMMENT ON COLUMN public.para_appl_status.level1 IS 'LEVEL1';
COMMENT ON COLUMN public.para_appl_status.level2 IS 'LEVEL2';
COMMENT ON COLUMN public.para_appl_status.level3 IS 'LEVEL3';
COMMENT ON COLUMN public.para_appl_status.effective_date IS 'EFFECTIVE DATE';
COMMENT ON COLUMN public.para_appl_status.active IS 'ACTIVE (0: ACTIVE, 1: NONACTIVE)';
COMMENT ON COLUMN public.para_appl_status.created_date IS 'CREATED DATE';
COMMENT ON COLUMN public.para_appl_status.created_by IS 'CREATED BY';
COMMENT ON COLUMN public.para_appl_status.modified_date IS 'MODIFIED DATE';
COMMENT ON COLUMN public.para_appl_status.modified_by IS 'MODIFIED BY';
