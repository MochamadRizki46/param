CREATE TABLE if not exists public.para_upld_grading_dealer (
	para_object_id varchar(5) NOT NULL,
	para_deal_outlet_id varchar(11) NOT NULL,
	para_deal_grade_id varchar(3) NOT NULL,
	para_eff_date timestamp NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_upld_grading_dealer_uk UNIQUE (para_object_id, para_deal_outlet_id, para_deal_grade_id, para_eff_date)
);