CREATE TABLE IF NOT EXISTS public.para_object_type (
	para_object_type_id varchar(5) NOT NULL,
	para_object_group_id varchar(5) NOT NULL,
	para_object_type_name varchar(50) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NOT NULL,
	CONSTRAINT para_object_type_pkey PRIMARY KEY (para_object_type_id)
);


