CREATE TABLE IF NOT EXISTS public.para_object_model (
	para_object_brand_id varchar(5) NOT NULL,
	para_object_type_id varchar(5) NOT NULL,
	para_object_genre_id varchar(5) NOT NULL,
	para_object_model_id varchar(5) NOT NULL,
	para_object_model_name varchar(50) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NOT NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	visible numeric NULL,
	CONSTRAINT para_object_model_pkey PRIMARY KEY (para_object_model_id)
);


