CREATE TABLE if not exists public.erp_bp_role (
	erp_bp_bpartner varchar(10) NULL,
	erp_bp_rolecategory varchar(10) NULL,
	erp_bp_request_number varchar(10) NULL,
	erp_bp_partnerexternal varchar(20) NULL,
	erp_bp_item_number numeric NULL,
	erp_bp_timestamp timestamp NULL,
	CONSTRAINT erp_bp_role_uk UNIQUE (erp_bp_bpartner, erp_bp_request_number, erp_bp_item_number)
);
