CREATE TABLE IF NOT EXISTS public.para_kecamatan (
	para_kabkot_id varchar(4) NOT NULL,
	para_kecamatan_id varchar(5) NOT NULL,
	para_kecamatan_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_kecamatan_pkey PRIMARY KEY (para_kecamatan_id)
);


