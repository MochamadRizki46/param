CREATE TABLE IF NOT EXISTS public.para_prod_matrix (
	para_object_id varchar(5) NULL,
	para_prod_type_id varchar(5) NULL,
	para_prod_matrix_id varchar(5) NOT NULL,
	para_prod_matrix_desc varchar(50) NULL,
	effective_date timestamp NULL,
	active int4 NULL,
	logid numeric NULL,
	created_date timestamp NULL,
	created_by varchar(20) NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_prod_matrix_pkey PRIMARY KEY (para_prod_matrix_id)
);


