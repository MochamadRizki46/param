CREATE TABLE if not exists public.para_document_type (
	para_document_type_id varchar(3) NOT NULL,
	para_document_type_name varchar(100) NOT NULL,
	effective_date timestamp NOT NULL,
	active int4 NOT NULL,
	logid numeric NULL,
	created_date timestamp NOT NULL,
	created_by varchar(20) NOT NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_document_type_pkey PRIMARY KEY (para_document_type_id)
);
