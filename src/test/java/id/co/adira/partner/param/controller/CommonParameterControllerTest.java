package id.co.adira.partner.param.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.param.dto.request.LOVBtmRequestDTO;
import id.co.adira.partner.param.dto.request.ModelRequestDTO;
import id.co.adira.partner.param.dto.request.TipeUnitRequestDTO;
import id.co.adira.partner.param.dto.response.*;
import id.co.adira.partner.param.service.CommonParameterService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static id.co.adira.partner.param.constant.AppEnum.SUCCESS;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CommonParameterControllerTest {
	@InjectMocks
	CommonParameterController commonParameterController;

	@Mock
	CommonParameterService commonParameterService;

	@Autowired
	MockMvc mockMvc;

	@Autowired
	WebApplicationContext context;

	ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetKelurahanByKelurahanNameAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		List<KecamatanDetailResponseDTO> kecamatanDetailResponseDTOS = new ArrayList<>();
		KecamatanDetailResponseDTO firstKecamatanDetailResponseDTO = new KecamatanDetailResponseDTO();

		firstKecamatanDetailResponseDTO.setKabKotId("KBKT001");
		firstKecamatanDetailResponseDTO.setKabKotDesc("first_kabKot");
		firstKecamatanDetailResponseDTO.setParaKelurahanIdKec("KC001");
		firstKecamatanDetailResponseDTO.setParaKecamatanDesc("first_kecamatan");
		firstKecamatanDetailResponseDTO.setKabKotIdProv("PRV001");
		firstKecamatanDetailResponseDTO.setProvinsiDesc("first_provinsi");
		firstKecamatanDetailResponseDTO.setParaKelurahanId("KL001");
		firstKecamatanDetailResponseDTO.setParaKelurahanDesc("first_kelurahan");
		firstKecamatanDetailResponseDTO.setZipCode("10000");

		kecamatanDetailResponseDTOS.add(firstKecamatanDetailResponseDTO);

		baseResponse.setData(kecamatanDetailResponseDTOS);
		when(commonParameterService.getKecamatanByKelurahanName("first_kelurahan")).thenReturn(baseResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/api/common/param/get-kelurahan?kelurahanName=first_kelurahan")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}

	@Test
	public void testGetKabKotAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		List<KabKotDetailResponseDTO> kabKotDetailResponseDTOS = new ArrayList<>();

		KabKotDetailResponseDTO firstKabKotDetail = new KabKotDetailResponseDTO();
		firstKabKotDetail.setProvinsiCode("PROV001");
		firstKabKotDetail.setCode("KBKT001");
		firstKabKotDetail.setDescription("first_kabkot");

		kabKotDetailResponseDTOS.add(firstKabKotDetail);

		KabKotDetailResponseDTO secondKabKotDetail = new KabKotDetailResponseDTO();
		secondKabKotDetail.setProvinsiCode("PROV002");
		secondKabKotDetail.setCode("KBKT002");
		secondKabKotDetail.setDescription("second_kabkot");

		kabKotDetailResponseDTOS.add(secondKabKotDetail);

		baseResponse.setData(kabKotDetailResponseDTOS);
		when(commonParameterService.getKabKotByName("kabkot")).thenReturn(baseResponse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/common/param/get-kabkot?kabKotName=kabkot")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}

	@Test
	public void testGetPortfolioByPartnerCodeAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		PortfolioHeadResponseDTO portfolioHeadResponseDTO = new PortfolioHeadResponseDTO();
		List<String> portfolios = new ArrayList<>();
		portfolios.add("NMCY");
		portfolios.add("UMCY");

		portfolioHeadResponseDTO.setPortfolios(portfolios);

		when(commonParameterService.getPortfolioByPartnerCode("PRT001")).thenReturn(baseResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/common/param/get-portfolio?partnerCode=PRT001")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void testGetKabKotByKabKotNameAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		KabKotHeadResponseDTO kabKotHeadResponseDTO = new KabKotHeadResponseDTO();
		List<KabKotDetailResponseDTO> kabKotDetailResponseDTOS = new ArrayList<>();

		KabKotDetailResponseDTO firstKabKotDetail = new KabKotDetailResponseDTO();
		firstKabKotDetail.setCode("KBKT001");
		firstKabKotDetail.setProvinsiCode("PROV001");
		firstKabKotDetail.setDescription("First Kabupaten Kota");

		kabKotDetailResponseDTOS.add(firstKabKotDetail);

		KabKotDetailResponseDTO secondKabKotDetail = new KabKotDetailResponseDTO();
		firstKabKotDetail.setCode("KBKT002");
		firstKabKotDetail.setProvinsiCode("PROV002");
		firstKabKotDetail.setDescription("Second Kabupaten Kota");

		kabKotDetailResponseDTOS.add(secondKabKotDetail);

		kabKotHeadResponseDTO.setKabkotList(kabKotDetailResponseDTOS);
		baseResponse.setData(kabKotHeadResponseDTO);

		when(commonParameterService.getKabKotByName("Kabupaten Kota")).thenReturn(baseResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/api/common/param/get-kabkot?kabKotName=Kabupaten Kota").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}

	@Test
	public void testGetTipeUnitAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		TipeUnitHeadResponseDTO tipeUnitHeadResponseDTO = new TipeUnitHeadResponseDTO();
		TipeUnitDetailResponseDTO tipeUnitDetailResponseDTO = new TipeUnitDetailResponseDTO();
		List<TipeUnitDetailResponseDTO> tipeUnitDetailResponseDTOS = new ArrayList<>();

		TipeUnitRequestDTO tipeUnitRequestDTO = new TipeUnitRequestDTO();
		tipeUnitRequestDTO.setObjectGroupId("OGU001");
		tipeUnitRequestDTO.setObjectBrandName("first_brand");

		tipeUnitDetailResponseDTO.setObjectTypeId("OT001");
		tipeUnitDetailResponseDTO.setObjectGenreId("OG001");
		tipeUnitDetailResponseDTO.setObjectBrandName("first_brand");
		tipeUnitDetailResponseDTO.setEffectiveDate(new Date());

		tipeUnitDetailResponseDTOS.add(tipeUnitDetailResponseDTO);
		tipeUnitHeadResponseDTO.setBrandName("Test Brand");
		tipeUnitHeadResponseDTO.setUnitList(tipeUnitDetailResponseDTOS);

		baseResponse.setData(tipeUnitHeadResponseDTO);

		String jsonStr = objectMapper.writeValueAsString(tipeUnitRequestDTO);

		when(commonParameterService.getTipeUnit(tipeUnitRequestDTO)).thenReturn(baseResponse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/common/param/get-tipe-unit")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}

	@Test
	public void testGetBrandPartnerAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		List<String> brandList = new ArrayList<>();
		brandList.add("Honda");
		brandList.add("Yamaha");

		baseResponse.setData(brandList);

		when(commonParameterService.getBrandPartnerByPartnerCode("PRT001")).thenReturn(baseResponse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/api/common/param/get-brand-partner?partnerCode=PRT001").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void testGetLovBrandTipeModelAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		LOVBtmRequestDTO lovBtmRequestDTO = new LOVBtmRequestDTO();
		lovBtmRequestDTO.setObjectId("OBJ001");
		lovBtmRequestDTO.setBrandName("Test Brand");

		LovBrandTipeModelHeadResponseDTO lovBrandTipeModelHeadResponseDTO = new LovBrandTipeModelHeadResponseDTO();
		List<LovBrandTipeModelDetailResponseDTO> brandTipeModels = new ArrayList<>();

		LovBrandTipeModelDetailResponseDTO lovBrandTipeModelDetailResponseDTO = new LovBrandTipeModelDetailResponseDTO();
		lovBrandTipeModelDetailResponseDTO.setObjectModelId("OM001");
		lovBrandTipeModelDetailResponseDTO.setObjectModelName("first_model");
		lovBrandTipeModelDetailResponseDTO.setObjectBrandId("OB001");
		lovBrandTipeModelDetailResponseDTO.setObjectBrandName("first_brand");
		lovBrandTipeModelDetailResponseDTO.setObjectTypeId("OT001");
		lovBrandTipeModelDetailResponseDTO.setObjectTypeName("first_type");
		lovBrandTipeModelDetailResponseDTO.setObjectName("first_object");

		brandTipeModels.add(lovBrandTipeModelDetailResponseDTO);
		lovBrandTipeModelHeadResponseDTO.setBrandTipeDetails(brandTipeModels);

		baseResponse.setData(lovBrandTipeModelHeadResponseDTO);

		when(commonParameterService.getLOVBrandTipeModel(lovBtmRequestDTO)).thenReturn(baseResponse);

		String jsonStr = objectMapper.writeValueAsString(lovBtmRequestDTO);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/common/param/get-lov-brand-tipe-model")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	//	@Test
//	public void testGetModelAndExpectStatusOk() throws Exception {
//		BaseResponse baseResponse = new BaseResponse();
//
//		baseResponse.setStatus(HttpStatus.OK);
//		baseResponse.setMessage(SUCCESS.getMessage());
//
//		ModelRequestDTO modelRequestDTO = new ModelRequestDTO();
//		modelRequestDTO.setSearch("brand");
//		modelRequestDTO.setDlc("000195");
//		modelRequestDTO.setGroupCode("001");
//		modelRequestDTO.setBranchId("BR001");
//
//		ObjectModelDetailHeadResponseDTO objectModelDetailHeadResponseDTO = new ObjectModelDetailHeadResponseDTO();
//		List<ObjectModelDetailDetailResponseDTO> objectModelDetailDetailResponseDTOS = new ArrayList<>();
//
//		ObjectModelDetailDetailResponseDTO firstObjectDetail = new ObjectModelDetailDetailResponseDTO();
//		firstObjectDetail.setObjectTypeCode("OBJT001");
//		firstObjectDetail.setObjectTypeDesc("first_object_type");
//		firstObjectDetail.setObjectModelCode("OBM001");
//		firstObjectDetail.setObjectModelDesc("first_object_model");
//		firstObjectDetail.setObjectBrandCode("OBC001");
//		firstObjectDetail.setObjectBrandDesc("first_brand");
//
//		objectModelDetailDetailResponseDTOS.add(firstObjectDetail);
//
//		ObjectModelDetailDetailResponseDTO secondObjectDetail = new ObjectModelDetailDetailResponseDTO();
//		secondObjectDetail.setObjectTypeCode("OBJT002");
//		secondObjectDetail.setObjectTypeDesc("second_object_type");
//		secondObjectDetail.setObjectModelCode("OBM002");
//		secondObjectDetail.setObjectModelDesc("second_object_model");
//		secondObjectDetail.setObjectBrandCode("OBC002");
//		secondObjectDetail.setObjectBrandDesc("second_brand");
//
//		objectModelDetailDetailResponseDTOS.add(secondObjectDetail);
//		objectModelDetailHeadResponseDTO.setModelDetails(objectModelDetailDetailResponseDTOS);
//
//		baseResponse.setData(objectModelDetailHeadResponseDTO);
//		when(commonParameterService.getModelDetailDurable("441")).thenReturn(baseResponse);
//
//		String jsonStr = objectMapper.writeValueAsString(modelRequestDTO);
//		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/common/param/get-model")
//				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);
//
//		mockMvc.perform(requestBuilder).andExpect(status().isOk());
//	}

	@Test
	public void testGetModelDetailAndExpectStatusOk() throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setStatus(HttpStatus.OK);
		baseResponse.setMessage(SUCCESS.getMessage());

		ObjectModelDetailHeadResponseDTO objectModelDetailHeadResponseDTO = new ObjectModelDetailHeadResponseDTO();
		List<ObjectModelDetailDetailResponseDTO> objectModelDetails = new ArrayList<>();

		ObjectModelDetailDetailResponseDTO firstObjectDetail = new ObjectModelDetailDetailResponseDTO();
		firstObjectDetail.setObjectTypeCode("OBJT001");
		firstObjectDetail.setObjectTypeDesc("first_object_type");
		firstObjectDetail.setObjectModelCode("OBM001");
		firstObjectDetail.setObjectModelDesc("first_object_model");
		firstObjectDetail.setObjectBrandCode("OBC001");
		firstObjectDetail.setObjectBrandDesc("first_brand");

		objectModelDetails.add(firstObjectDetail);
		objectModelDetailHeadResponseDTO.setModelDetails(objectModelDetails);

		baseResponse.setData(objectModelDetailHeadResponseDTO);

		when(commonParameterService.getModelDetailDurable("first_brand")).thenReturn(baseResponse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/api/common/param/get-model-detail-durable?objectModelId=OBM001")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
}
