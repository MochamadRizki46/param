package id.co.adira.partner.param.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.param.dto.request.TipeUnitRequestDTO;
import id.co.adira.partner.param.dto.response.*;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectModelDetailRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaProvinsiRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaUserDetailRepository;
import id.co.adira.partner.param.repository.credsimparamrepo.ParaObjectModelRepository;
import id.co.adira.partner.param.util.ObjectConverter;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static id.co.adira.partner.param.constant.AppEnum.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CommonParameterServiceTest {
    @InjectMocks
    CommonParameterService commonParameterService;

    @Mock
    ParaProvinsiRepository paraProvinsiRepository;

    @Mock
    ParaUserDetailRepository paraUserDetailRepository;

    @Mock
    ParaObjectModelRepository paraObjectModelRepository;

    @Mock
    ParaObjectModelDetailRepository paraObjectModelDetailRepository;

    @Spy
    ObjectConverter objectConverter;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetKecamatanByKelurahanNameAndExpectDataIsExists() throws IOException {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());

        KecamatanResponseDTO kecamatanResponseDTO = new KecamatanResponseDTO();
        List<KecamatanDetailResponseDTO> kecamatanDetailResponseDTOS = new ArrayList<>();

        KecamatanDetailResponseDTO firstKecamatan = new KecamatanDetailResponseDTO();
        firstKecamatan.setKabKotId("KBKT001");
        firstKecamatan.setKabKotDesc("dummy_kabkot");
        firstKecamatan.setParaKelurahanId("KEL001");
        firstKecamatan.setParaKelurahanDesc("dummy_kelurahan");
        firstKecamatan.setParaKelurahanIdKec("KEC001");
        firstKecamatan.setParaKelurahanDesc("dummy_kelurahan");
        firstKecamatan.setKabKotIdProv("PROV001");
        firstKecamatan.setProvinsiDesc("dummy_provinsi");
        firstKecamatan.setParaKelurahanIdKec("KEC001");
        firstKecamatan.setParaKelurahanDesc("dummy_kecamatan");
        firstKecamatan.setZipCode("14045");

        kecamatanDetailResponseDTOS.add(firstKecamatan);
        kecamatanResponseDTO.setLocationDetails(kecamatanDetailResponseDTOS);

        baseResponse.setData(kecamatanResponseDTO);
        String baseResponseInString = objectMapper.writeValueAsString(baseResponse);

        when(paraProvinsiRepository.getLocationDetailsByKelurahanName("kelurahan")).thenReturn(kecamatanDetailResponseDTOS);
        when(objectConverter.convertObjectToString(baseResponse)).thenReturn(baseResponseInString);

        BaseResponse response = commonParameterService.getKecamatanByKelurahanName("kelurahan");
        KecamatanResponseDTO newKecamatanResponse = (KecamatanResponseDTO) response.getData();

        assertEquals(1, newKecamatanResponse.getLocationDetails().size());
    }

    @Test(expected = Exception.class)
    public void testGetKecamatanByKelurahanNameAndExpectThrowException() throws JsonProcessingException {

        when(paraProvinsiRepository.getLocationDetailsByKelurahanName("test")).thenThrow(Exception.class);

        BaseResponse response = commonParameterService.getKecamatanByKelurahanName("test");
        String dataResponse = objectMapper.writeValueAsString(response.getData());

        assertEquals(dataResponse, THERE_SOMETHING_ERROR_IN_OUR_SYSTEM.getMessage());
    }

    @Test
    public void testGetPortfolioAndExpectDataIsExists(){

        PortfolioHeadResponseDTO portfolioHeadResponseDTO = new PortfolioHeadResponseDTO();
        List<String> portfolios = new ArrayList<>();
        portfolios.add("first_porto");
        portfolios.add("second_porto");

        portfolioHeadResponseDTO.setPortfolios(portfolios);
        portfolioHeadResponseDTO.setPartnerCode("PRT001");

        when(paraUserDetailRepository.getPortfolioByPartnerCode("PRT001")).thenReturn(portfolios);
        BaseResponse baseResponse = commonParameterService.getPortfolioByPartnerCode("PRT001");

        PortfolioHeadResponseDTO responseDTO = (PortfolioHeadResponseDTO) baseResponse.getData();
        assertEquals(2, responseDTO.getPortfolios().size());
    }

    @Test
    public void testGetKabKotAndExpectDataExists(){
        List<KabKotDetailResponseDTO> kabKotDetailResponseDTOS = new ArrayList<>();

        KabKotDetailResponseDTO firstKabKotDetail = new KabKotDetailResponseDTO("PRV001", "CD001", "first_province");
        kabKotDetailResponseDTOS.add(firstKabKotDetail);

        KabKotDetailResponseDTO secondKabKotDetail = new KabKotDetailResponseDTO("PRV002", "CD002", "second_province");
        kabKotDetailResponseDTOS.add(secondKabKotDetail);

        when(paraProvinsiRepository.getLocationDetailsKabKotByKabKotName("province")).thenReturn(kabKotDetailResponseDTOS);
        BaseResponse baseResponse = commonParameterService.getKabKotByName("province");

        List<KabKotDetailResponseDTO> responseDTOS = (List<KabKotDetailResponseDTO>) baseResponse.getData();
        assertEquals(2, responseDTOS.size());
    }

    @Test
    public void testGetTipeUnitAndExpectDataExistsAndReturn(){
        TipeUnitHeadResponseDTO tipeUnitHeadResponseDTO = new TipeUnitHeadResponseDTO();
        List<TipeUnitDetailResponseDTO> unitDetailResponseDTOS = new ArrayList<>();

        TipeUnitRequestDTO tipeUnitRequestDTO = new TipeUnitRequestDTO("dummy_brand", "OBG001");

        TipeUnitDetailResponseDTO firstTipeUnit = new TipeUnitDetailResponseDTO("OBT001", "OBG001", "first_brand", new Date());
        unitDetailResponseDTOS.add(firstTipeUnit);

        TipeUnitDetailResponseDTO secondTipeUnit = new TipeUnitDetailResponseDTO("OBT002", "OBG002", "second_brand", new Date());
        unitDetailResponseDTOS.add(secondTipeUnit);

        tipeUnitHeadResponseDTO.setBrandName("dummy_brand");
        tipeUnitHeadResponseDTO.setUnitList(unitDetailResponseDTOS);

        when(paraObjectModelRepository.findTipeUnitByBrandNameAndObjectCode("dummy_brand", "OBG001")).thenReturn(unitDetailResponseDTOS);
        BaseResponse baseResponse = commonParameterService.getTipeUnit(tipeUnitRequestDTO);

        TipeUnitHeadResponseDTO tipeUnitHead = (TipeUnitHeadResponseDTO) baseResponse.getData();
        assertEquals(2, tipeUnitHead.getUnitList().size());
    }

    @Test
    public void testGetBrandPartnerByPartnerCodeAndExpectDataExistsAndReturnTwoDatas(){
        List<String> brands = new ArrayList<>();
        brands.add("YAHAMA");
        brands.add("HODNA");

        when(paraUserDetailRepository.getBrandByPartnerCode("PA001")).thenReturn(brands);
        BaseResponse baseResponse = commonParameterService.getBrandPartnerByPartnerCode("PA001");
        BrandResponseDTO brandResponseDTO = (BrandResponseDTO) baseResponse.getData();

        assertEquals(2, brandResponseDTO.getBrandList().size());

    }

    @Test
    public void testGetModelDetailAndExpectRetrievingData(){
        List<ObjectModelDetailDurableResponseDTO> objectModelDetailDetailResponseDTOS = new ArrayList<>();

        ObjectModelDetailDurableResponseDTO firstObjectModelDetail = new ObjectModelDetailDurableResponseDTO("OMC001", "OBM001", "OMD001", "dummy_model_detail");
        objectModelDetailDetailResponseDTOS.add(firstObjectModelDetail);

        ObjectModelDetailDurableResponseDTO secondObjectModelDetail = new ObjectModelDetailDurableResponseDTO("OMC001", "OBM001", "OMD001", "dummy_model_detail");
        objectModelDetailDetailResponseDTOS.add(secondObjectModelDetail);

        when(paraObjectModelDetailRepository.getModelDetailDurableOnly("OMC001")).thenReturn(objectModelDetailDetailResponseDTOS);
        BaseResponse baseResponse = commonParameterService.getModelDetailDurable("OMC001");

        List<ObjectModelDetailDurableResponseDTO> responseDTO = (List<ObjectModelDetailDurableResponseDTO>) baseResponse.getData();
        assertEquals(responseDTO.size(), 2);
    }

}
