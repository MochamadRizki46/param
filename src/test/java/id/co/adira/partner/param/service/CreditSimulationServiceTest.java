package id.co.adira.partner.param.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.adira.partner.param.dto.request.BiayaAdminRequestDTO;
import id.co.adira.partner.param.dto.response.BaseResponse;
import id.co.adira.partner.param.dto.response.BiayaAdminResponseDTO;
import id.co.adira.partner.param.repository.commonparamrepo.ParaKabKotRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectBrandRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectGroupRepository;
import id.co.adira.partner.param.repository.commonparamrepo.ParaObjectTypeRepository;
import id.co.adira.partner.param.repository.credsimparamrepo.*;
import id.co.adira.partner.param.util.ObjectConverter;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CreditSimulationServiceTest {

    @InjectMocks
    CreditSimulationParameterService creditSimulationParameterService;

    @Mock
    ParaUploadAdminFeeRepository paraUploadAdminFeeRepository;
    @Mock
    ParaUploadLandingRateRepository paraUploadLandingRateRepository;
    @Mock
    ParaProductProgramRepository paraProductProgramRepository;
    @Mock
    ParaUploadProfitFeeRepository paraUploadProfitFeeRepository;
    @Mock
    ParaUploadMarketPriceRepository paraUploadMarketPriceRepository;
    @Mock
    ParaObjectModelRepository paraObjectModelRepository;
    @Mock
    ParaKabKotRepository paraKabKotRepository;
    @Mock
    ParaObjectGroupRepository paraObjectGroupRepository;
    @Mock
    ParaObjectTypeRepository paraObjectTypeRepository;
    @Mock
    ParaObjectBrandRepository paraObjectBrandRepository;
    @Spy
    ObjectConverter objectConverter;
    @Autowired
    MockMvc mockMvc;
    @Autowired
    WebApplicationContext context;


    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetBiayaAdminAndExpectDataExists() throws JsonProcessingException {

        BiayaAdminRequestDTO biayaAdminRequestDTO = new BiayaAdminRequestDTO();
        biayaAdminRequestDTO.setProgramId("PRG001");
        biayaAdminRequestDTO.setTenor("12");
        biayaAdminRequestDTO.setObjectModelId("OBM001");
        biayaAdminRequestDTO.setObjectCode("OBC001");

        BiayaAdminResponseDTO biayaAdminResponseDTO = new BiayaAdminResponseDTO();
        biayaAdminResponseDTO.setAdminCashFee(BigDecimal.ZERO);
        biayaAdminResponseDTO.setAdminCreditFee(BigDecimal.ZERO);

        when(paraObjectGroupRepository.getParaObjectGroupByParaObjectId(biayaAdminRequestDTO.getObjectCode())).thenReturn("OBG001");
        when(paraUploadProfitFeeRepository.findBiayaAdminCashAndCreditForAutomotive
                (biayaAdminRequestDTO.getProgramId(), biayaAdminRequestDTO.getObjectModelId(),
                        new BigDecimal(biayaAdminRequestDTO.getTenor()), "OBG001"))
                .thenReturn(biayaAdminResponseDTO);

        BaseResponse baseResponse = creditSimulationParameterService.getBiayaAdmin(biayaAdminRequestDTO);
        BiayaAdminResponseDTO responseDTO = (BiayaAdminResponseDTO) baseResponse.getData();

        assertEquals(responseDTO.getAdminCreditFee(), biayaAdminResponseDTO.getAdminCreditFee());

    }

    @Test
    public void testGetBiayaAdminWhenObjectGroupIdIsNotFound() throws JsonProcessingException {
        BiayaAdminRequestDTO biayaAdminRequestDTO = new BiayaAdminRequestDTO();
        biayaAdminRequestDTO.setProgramId("PRG001");
        biayaAdminRequestDTO.setTenor("12");
        biayaAdminRequestDTO.setObjectModelId("OBM001");
        biayaAdminRequestDTO.setObjectCode("OBC002");

        BiayaAdminResponseDTO biayaAdminResponseDTO = new BiayaAdminResponseDTO();
        biayaAdminResponseDTO.setAdminCashFee(BigDecimal.ZERO);
        biayaAdminResponseDTO.setAdminCreditFee(new BigDecimal(10));

        when(paraObjectGroupRepository.getParaObjectGroupByParaObjectId(biayaAdminRequestDTO.getObjectCode())).thenReturn("");

        BaseResponse baseResponse = creditSimulationParameterService.getBiayaAdmin(biayaAdminRequestDTO);
        BiayaAdminResponseDTO responseDTO = (BiayaAdminResponseDTO) baseResponse.getData();

        assertEquals(responseDTO.getAdminCreditFee(), new BigDecimal(0));
    }
}
