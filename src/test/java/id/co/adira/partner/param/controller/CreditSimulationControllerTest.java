package id.co.adira.partner.param.controller;

import id.co.adira.partner.param.dto.request.*;
import id.co.adira.partner.param.dto.response.*;
import id.co.adira.partner.param.service.CreditSimulationParameterService;
import id.co.adira.partner.param.util.ObjectConverter;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static id.co.adira.partner.param.constant.AppEnum.SUCCESS;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CreditSimulationControllerTest {

    @InjectMocks
    CreditSimulationParameterController creditSimulationParameterController;

    @Mock
    CreditSimulationParameterService creditSimulationParameterService;

    @Autowired
    ObjectConverter objectConverter;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @BeforeEach
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetInsuranceRateAPIAndExpectStatusIsOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();

        InsuranceRateResponseDTO insuranceRateResponseDTO = new InsuranceRateResponseDTO();
        insuranceRateResponseDTO.setInsuranceRate(10.0);
        insuranceRateResponseDTO.setProgramId("PRG001");

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(insuranceRateResponseDTO);

        when(creditSimulationParameterService.getInsuranceRateByProgramId("PRG001")).thenReturn(baseResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/credsim/param/get-insurance-rate?programId=PRG001")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }


    @Test
    public void testGetAllDataProgramAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());

        ParaProgramListResponseDTO paraProgramListResponseDTO = new ParaProgramListResponseDTO();
        List<ParaProgramResponseDTO> paraProgramResponseDTOS = new ArrayList<>();

        ParaProgramResponseDTO firstParaProgramResponseDTO = new ParaProgramResponseDTO();
        firstParaProgramResponseDTO.setParaProdProgramId("PPID001");
        firstParaProgramResponseDTO.setEffectiveDate("2022-01-03");
        firstParaProgramResponseDTO.setParaProdProgramName("First Program");

        paraProgramResponseDTOS.add(firstParaProgramResponseDTO);

        ParaProgramResponseDTO secondParaProgramResponseDTO = new ParaProgramResponseDTO();
        secondParaProgramResponseDTO.setParaProdProgramId("PPID002");
        secondParaProgramResponseDTO.setParaProdProgramName("2022-01-03");
        secondParaProgramResponseDTO.setParaProdProgramName("Second Program");

        paraProgramResponseDTOS.add(secondParaProgramResponseDTO);
        paraProgramListResponseDTO.setPrograms(paraProgramResponseDTOS);

        baseResponse.setData(paraProgramListResponseDTO);

        when(creditSimulationParameterService.getAllDataProgram()).thenReturn(baseResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/credsim/param/get-all-data-program")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
        
    }

    @Test
    public void testGetDataProgramByProgramIdAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());

        ParaProgramResponseDTO paraProgramResponseDTO = new ParaProgramResponseDTO();
        paraProgramResponseDTO.setParaProdProgramId("PPID001");
        paraProgramResponseDTO.setEffectiveDate("2022-01-03");
        paraProgramResponseDTO.setParaProdProgramName("Program Name");

        baseResponse.setData(paraProgramResponseDTO);

        when(creditSimulationParameterService.getDataProgramByProgramId("PPID001")).thenReturn(baseResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/credsim/param/get-data-program?programId=PPID001")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }



    @Test
    public void testGetEffectiveOrLandingRateAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();

        EffectiveOrLandingRateRequestDTO effectiveOrLandingRateRequestDTO = new EffectiveOrLandingRateRequestDTO();
        effectiveOrLandingRateRequestDTO.setObjectModelId("OBM001");
        effectiveOrLandingRateRequestDTO.setObjectId("OBJ001");
        effectiveOrLandingRateRequestDTO.setTenor(new BigDecimal(12));
        effectiveOrLandingRateRequestDTO.setPic("user@ad1gate.com");
        effectiveOrLandingRateRequestDTO.setObjectTypeId("OBJT001");

        EffectiveOrLandingRateResponseDTO effectiveOrLandingRateResponseDTO = new EffectiveOrLandingRateResponseDTO();
        effectiveOrLandingRateResponseDTO.setEffectiveOrLandingRate(new BigDecimal(12.20));

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());
        baseResponse.setData(effectiveOrLandingRateResponseDTO);

        String jsonStr = objectConverter.convertObjectToString(effectiveOrLandingRateRequestDTO);
        when(creditSimulationParameterService.getEffectiveOrLandingRate(effectiveOrLandingRateRequestDTO)).thenReturn(baseResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/credsim/param/get-effective-or-landing-rate")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

   

    @Test
    public void testGetOtrAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage(SUCCESS.getMessage());

        DpValueForDanaTunaiRequestDTO otrRequestDTO = new DpValueForDanaTunaiRequestDTO();
        otrRequestDTO.setObjectGroupId("OBJ001");
        otrRequestDTO.setObjectBrandId("OBR001");
        otrRequestDTO.setObjectTypeId("OTY001");
        otrRequestDTO.setObjectModelId("OBM001");
        otrRequestDTO.setTenor(new BigDecimal(12));
        otrRequestDTO.setManufacturerYear(new BigDecimal(2022));

        MarketPriceResponseDTO marketPriceResponseDTO = new MarketPriceResponseDTO();
        marketPriceResponseDTO.setObjectTypeName("dummy_object_type");
        marketPriceResponseDTO.setObjectGroupName("dummy_group");
        marketPriceResponseDTO.setObjectModelName("dummy_model");
        marketPriceResponseDTO.setObjectBrandName("dummy_brand");
        marketPriceResponseDTO.setManufacturerYear(new BigDecimal(2022));


        baseResponse.setData(marketPriceResponseDTO);
        when(creditSimulationParameterService.getDpValueForDatun(otrRequestDTO)).thenReturn(baseResponse);

        String jsonStr = objectConverter.convertObjectToString(otrRequestDTO);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/credsim/param/get-dp-value-for-datun")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

//    @Test
//    public void testCalculateCicilanAndExpectStatusOk() throws Exception {
//        BaseResponse baseResponse = new BaseResponse();
//
//        baseResponse.setStatus(HttpStatus.OK);
//        baseResponse.setMessage(SUCCESS.getMessage());
//
//        CalculateCicilanRequestDTO calculateCicilanRequestDTO = new CalculateCicilanRequestDTO();
//        calculateCicilanRequestDTO.setTenor(12);
//        calculateCicilanRequestDTO.setValuePencairan(new BigDecimal(60000));
//
//        String jsonStr = objectConverter.convertObjectToString(calculateCicilanRequestDTO);
//
//        CalculateCicilanResponseDTO calculateCicilanResponseDTO = new CalculateCicilanResponseDTO();
//        calculateCicilanResponseDTO.setTenor(calculateCicilanRequestDTO.getTenor());
//        calculateCicilanResponseDTO.setValuePencairan(new BigDecimal(60000));
//        calculateCicilanResponseDTO.setInstalment(new BigDecimal(5000));
//
//        baseResponse.setData(calculateCicilanResponseDTO);
//
//        when(creditSimulationParameterService.calculateCicilan(calculateCicilanRequestDTO)).thenReturn(baseResponse);
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/credsim/param/calculate-cicilan")
//                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);
//
//        mockMvc.perform(requestBuilder).andExpect(status().isOk());
//    }
}
